DO $$ DECLARE
  r RECORD;
BEGIN
  FOR r IN (SELECT tablename FROM pg_tables WHERE schemaname = current_schema()) LOOP
    EXECUTE 'DROP TABLE ' || quote_ident(r.tablename) || ' CASCADE';
  END LOOP;
END $$;

create database loyaltyprogramdw;

-- utility
CREATE TABLE branch (
	 id SERIAL PRIMARY KEY,
	 name VARCHAR(255),
	 address VARCHAR(255),
	 capacity INT,
	 office_hour VARCHAR(255)
);
CREATE UNIQUE INDEX UI_branch
ON branch (id,name,address,capacity,office_hour);

CREATE TABLE gender (
	 id SERIAL PRIMARY KEY,
	 gender VARCHAR(255)
);
CREATE UNIQUE INDEX UI_gender
ON gender (id, gender);

CREATE TABLE users (
	 id SERIAL PRIMARY KEY,
	 date_of_birth DATE,
	 gender VARCHAR(255)
);
CREATE UNIQUE INDEX UI_users
ON users (id,date_of_birth,gender);

CREATE TABLE customer (
	 id SERIAL PRIMARY KEY,
	 date_of_birth DATE,
	 gender VARCHAR(255)
);
CREATE UNIQUE INDEX UI_customer
ON customer (id,date_of_birth,gender);

CREATE TABLE agent (
	id SERIAL PRIMARY KEY,
	each_appointment_commission INT,
	each_referral_commission INT,
	employed_data Date,
	level INT
);
CREATE UNIQUE INDEX UI_agent
ON agent (id,each_appointment_commission,each_referral_commission,employed_data,level);

CREATE TABLE treatment (
	 id SERIAL PRIMARY KEY,
	 detail VARCHAR(255),
	 title VARCHAR(255),
	 default_score INT
);
CREATE UNIQUE INDEX UI_treatment
ON treatment (id,detail, title,default_score);

----------------------------------------------------------------------------
-- appointment star
-- CREATE TABLE treatment (
-- );

-- CREATE TABLE branch (
-- );

-- CREATE TABLE users (
-- );

-- CREATE TABLE appointed_score (
-- );

CREATE TABLE score (
	 id SERIAL PRIMARY KEY,
	 score INT,
	 appointment_id INT,
	 treatments_id INT
);
CREATE UNIQUE INDEX UI_score
ON score (id,score,appointment_id,treatments_id);

CREATE TABLE appointed_earned_point (
	 id SERIAL PRIMARY KEY,
	 point INT,
	 customer_id INT,
	appointment_id INT
);
CREATE UNIQUE INDEX UI_appointed_earned_point
ON appointed_earned_point (id,point,customer_id);

CREATE TABLE  appointed_commission(
	 id SERIAL PRIMARY KEY,
	 points INT,
	 agent_id INT,
	appointment_id INT
);
CREATE UNIQUE INDEX UI_appointed_commission
ON appointed_commission (id,points,agent_id);

CREATE TABLE fact_appointments (
    id SERIAL PRIMARY KEY,
	treatment_id INT,
	    FOREIGN KEY (treatment_id) REFERENCES treatment(id),
	branch_id INT,
	    FOREIGN KEY (branch_id) REFERENCES branch(id),
	customer_id INT,
	    FOREIGN KEY (customer_id) REFERENCES customer(id),
	score_id INT,
	    FOREIGN KEY (score_id) REFERENCES score(id),
	appointed_earned_point_id INT,
	    FOREIGN KEY (appointed_earned_point_id) REFERENCES appointed_earned_point(id),
	appointed_commission_id INT,
	    FOREIGN KEY (appointed_commission_id) REFERENCES appointed_commission(id),
	create_time timestamp DEFAULT CURRENT_TIMESTAMP,
	update_time timestamp DEFAULT CURRENT_TIMESTAMP
);



----------------------------------------------------------------------------
-- purchase star
CREATE TABLE purchase_info (
	id SERIAL PRIMARY KEY,
	purchase_amount INT,
	benefit_amount INT,
	treatment_id INT,
	customer_id INT
);

-- CREATE TABLE customer (
-- );
-- CREATE TABLE treatment (
-- );





CREATE TABLE fact_purchase (
    id SERIAL PRIMARY KEY,
	purchase_info_id INT,
	    FOREIGN KEY (purchase_info_id) REFERENCES purchase_info(id),
	customer_id INT,
	    FOREIGN KEY (customer_id) REFERENCES customer(id),
	treatment_id INT,
	    FOREIGN KEY (treatment_id) REFERENCES treatment(id),
	create_time timestamp DEFAULT CURRENT_TIMESTAMP,
	update_time timestamp DEFAULT CURRENT_TIMESTAMP
);

















-- reward star
CREATE TABLE reward_info (
	 id SERIAL PRIMARY KEY,
	 detail VARCHAR(255),
	 name VARCHAR(255),
	 redeem_points INT
)
CREATE UNIQUE INDEX UI_reward_info
ON reward_info (id,detail,name,redeem_points);

CREATE TABLE reward_type (
	 id SERIAL PRIMARY KEY,
	 type VARCHAR(255),
)
CREATE UNIQUE INDEX UI_reward_type
ON reward_type (id,type);

CREATE TABLE pickup_location_reward_relation (
	 id SERIAL PRIMARY KEY,
	 type VARCHAR(255),
)
CREATE UNIQUE INDEX UI_pickup_location_reward_relation
ON pickup_location_reward_relation (id,type);







CREATE TABLE fact_reward (
    id SERIAL PRIMARY KEY,
	reward_info_id INT,
	    FOREIGN KEY (user_id) REFERENCES users(id),    
	reward_type_id INT,
	    FOREIGN KEY (user_id) REFERENCES users(id),    
	create_time timestamp DEFAULT CURRENT_TIMESTAMP,
	update_time timestamp DEFAULT CURRENT_TIMESTAMP,
	old_version boolean DEFAULT false
);


-- users star
create table user_info (
    id SERIAL PRIMARY KEY,
	username VARCHAR(255),
	
)


CREATE TABLE customer (
    id SERIAL PRIMARY KEY,
	user_info_id INT,
	    FOREIGN KEY (user_id) REFERENCES users(id),    
	customer_appointment_id INT,
	    FOREIGN KEY (user_id) REFERENCES users(id),    
	redeem_order_id INT,
	    FOREIGN KEY (user_id) REFERENCES users(id),    
	redeem_item_history_id INT,
	    FOREIGN KEY (user_id) REFERENCES users(id),    
	cart_added_item_id INT,
	    FOREIGN KEY (user_id) REFERENCES users(id),    
	cart_removed_item_id INT,
	    FOREIGN KEY (user_id) REFERENCES users(id),    
	cart_checked_out_item_id INT,
	    FOREIGN KEY (user_id) REFERENCES users(id),    
	create_time timestamp DEFAULT CURRENT_TIMESTAMP,
	update_time timestamp DEFAULT CURRENT_TIMESTAMP
);
