import {FontAwesomeIcon} from "@fortawesome/react-native-fontawesome";
import React,{useEffect,useState} from 'react';
import {View,Text,TouchableOpacity,useColorScheme,Platform,Linking,TextInput,TouchableWithoutFeedback,Keyboard,FlatList,
ScrollView}from 'react-native';
import FontAwesome from "react-native-vector-icons/FontAwesome";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import Ionicons from "react-native-vector-icons/Ionicons";
import {Colors} from "react-native/Libraries/NewAppScreen";
import {useDispatch,useSelector} from "react-redux";
import {ContactCard} from "../models";
import {IRootState} from "../store";
import {callNull,addNewContactNull,setWhatsAppNull} from "./redux/agentCallList/actions";
import {callSuccess,callFail,getAllCallList,addNewContact} from "./redux/agentCallList/thunks";
import {styles} from './styles/AgentCallListScreenStyles';

function checkMobile(phone:string){
	for(let i = 0; i < phone.length; i++){
		if(isNaN(parseInt(phone[i]))){
			return false
		}
	}
	return true
}

function AgentCallListScreen() {
	const isDarkMode = useColorScheme() === "dark";
	const dispatch = useDispatch();
	const [phone,setPhone] = useState(98765432);
	const contactSuccess = useSelector((state:IRootState)=>state.agentCallListPage.call);
	const contactList = useSelector((state:IRootState)=>state.agentCallListPage.contactList);
	const [addPhone,setAddPhone] = useState(false);
	const [newPhoneNumber,setNewPhoneNumber] = useState("");
	const newContact = useSelector((state:IRootState)=>state.agentCallListPage.addNewContact)
	const whatsApp = useSelector((state:IRootState)=>state.agentCallListPage.canWhatsApp)
	// console.log(contactList)
	useEffect(() => {
		dispatch(getAllCallList())
	},[dispatch])

	useEffect(() => {
		let isNumber = checkMobile(newPhoneNumber);
		if(whatsApp && isNumber && newPhoneNumber.length === 8){
			Linking.openURL(`https://wa.me/852${newPhoneNumber}?text=你好,現時Kson美容有美容優惠,現時申請成為會員即享有優惠&app_absent=0`)
		}
	},[whatsApp])

    return(
		<TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
		<View style={[styles.flexContainer,{backgroundColor:Colors.white}]}>
            <View style={styles.contentContainer}>
                <View style={styles.newContactContainer}>
                    <View style={styles.header}>
                        <Text style={styles.subHead}>新聯絡人</Text>
						<TouchableOpacity onPress={() => {
							setPhone(Math.floor(Math.random()*100000000))
							dispatch(callNull())
						}}>
							<Text style={styles.grey}>提供新客戶</Text>
						</TouchableOpacity>
                    </View>
                    <View style={styles.newContactRow}>
                        <View>
                            {/* <Text style={styles.newContact}>姓名: Mr K</Text> */}
							<Text>號碼: {phone}</Text>
                        </View>
						<View style={styles.height}>
                        {contactSuccess === null && <View style={styles.buttonContainer}>
                            <TouchableOpacity style={styles.successButton} onPress={() => {
								// dispatch(callSuccess(phone))
								let phoneNumber = "";
								if(Platform.OS === "android"){
									phoneNumber = `tel:${phone}`;
								}else{
									phoneNumber = `telprompt:${phone}`;
								}
								Linking.openURL(phoneNumber);
							}}>
                                {/* <Text style={styles.buttonText}>聯絡成功</Text> */}
								<FontAwesome name="phone" color="#ffffff" size={24}/>
                            </TouchableOpacity>
							<TouchableOpacity style={styles.whatsAppButton} onPress={() => {
								Linking.openURL(`https://wa.me/852${phone}?text=你好,現時Kson美容有美容優惠,現時申請成為會員即享有優惠&app_absent=0`)
								dispatch(callSuccess(phone))
							}}>
                                {/* <Text style={styles.buttonText}>聯絡成功</Text> */}
								<Ionicons name="logo-whatsapp" color="#ffffff" size={24}/>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.failButton} onPress={() => {
								dispatch(callFail(phone))
							}}>
                                {/* <Text style={styles.buttonText}>聯絡失敗</Text> */}
								<FontAwesome5 name="times"color="#ffffff" size={28}/>
                            </TouchableOpacity>
                        </View>}
						{contactSuccess && <Text style={styles.success}>聯絡成功</Text>}
						{contactSuccess === false && <Text style={styles.fail}>聯絡失敗</Text>}
						</View>
                    </View>
                </View>
                <View style={styles.contactListContainer}>
                    <View style={styles.newHeadRow}>
                        <Text style={styles.subHead}>我的聯絡人</Text>
                        <TouchableOpacity style={styles.iconContainer} onPress={() => {
							setAddPhone(!addPhone)
							dispatch(addNewContactNull())
							setNewPhoneNumber("")
							dispatch(setWhatsAppNull())
						}}>
							<FontAwesomeIcon style={styles.icon} icon="plus" size={20} color={"#cccccc"}/>
                        </TouchableOpacity>
                    </View>
					{addPhone && <View>
					<Text style={styles.whatsApp}>如果輸入號碼為未現有客戶,將會彈出傳送whatsApp給相應客戶的視窗</Text>
					<View style={styles.addContainer}>
						<View style={styles.phoneTextContainer}>
							<Text style={styles.phoneText}>電話</Text>
							<View style={styles.search}>
								<TextInput value={newPhoneNumber} style={styles.newPhoneContainer}
								onChangeText={setNewPhoneNumber}/>
							</View>
						</View>
						{newContact === null && <TouchableOpacity style={styles.clickButton} onPress={() => {
							let isNumber = checkMobile(newPhoneNumber);
							{isNumber && newPhoneNumber.length === 8 && dispatch(addNewContact(parseInt(newPhoneNumber)))}
						}}>
							<Text style={styles.clickButtonText}>送出</Text>
						</TouchableOpacity>}
						{newContact && <Text style={styles.success}>成功送出</Text>}
						{newContact === false && <Text style={styles.fail}>未能送出</Text>}
					</View>
					</View>}
                    <View style={addPhone ? styles.moreContactList : styles.contactList}>
					{contactList.length > 0 &&
					<FlatList<ContactCard> data={contactList} renderItem={(row)=>{
						let id = row.item.id
						const today = new Date().toISOString().slice(0,10);
						let age;
						if((parseInt(today.slice(6,8)) - parseInt(row.item.date_of_birth.slice(6,8)) < 0)){
							age = parseInt(today.slice(0,4)) - parseInt(row.item.date_of_birth.slice(0,4)) - 1
						}else if((parseInt(today.slice(6,8)) - parseInt(row.item.date_of_birth.slice(6,8))) === 0){
							if((parseInt(today.slice(10,12)) - parseInt(row.item.date_of_birth.slice(10,12)) < 0)){
								age = parseInt(today.slice(0,4)) - parseInt(row.item.date_of_birth.slice(0,4)) - 1
							}else{
								age = parseInt(today.slice(0,4)) - parseInt(row.item.date_of_birth.slice(0,4))
							}
						}else{
							age = parseInt(today.slice(0,4)) - parseInt(row.item.date_of_birth.slice(0,4))
						}
						return(
						<View key={id} style={styles.contactRow}>
							<View style={styles.contactTextContainer}>
								<View style={styles.contactTextRow}>
									<Text style={styles.text}>姓名: {row.item.display_name}</Text>
								</View>
								<View style={styles.contactTextRow}>
									<Text style={styles.text}>年齡: {age}</Text>
								</View>
								<View style={styles.contactTextRow}>
									<Text style={styles.text}>性別: {row.item.gender}</Text>
								</View>
							</View>
							<View style={styles.buttonContainer}>
								<TouchableOpacity style={styles.button} onPress={() => {
									let phoneNumber = "";
									if(Platform.OS === "android"){
										phoneNumber = `tel:${row.item.mobile}`;
									}else{
										phoneNumber = `telprompt:${row.item.mobile}`;
									}
									Linking.openURL(phoneNumber);
								}}>
									<Text style={styles.buttonText}>聯絡</Text>
								</TouchableOpacity>
							</View>
						</View>
						)
					}}/>
					}
                    </View>
                </View>
            </View>
        </View>
		</TouchableWithoutFeedback>
    )
}

export default AgentCallListScreen;