import {RouteProp,useNavigation,useRoute} from "@react-navigation/native";
import React,{useState} from "react";
import {View,Image,Text,useColorScheme,TouchableOpacity} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import {Colors} from "react-native/Libraries/NewAppScreen";
import {useDispatch,useSelector} from "react-redux";
import envfile from "../envfile";
import {ParamList} from "../models";
import {IRootState} from "../store";
import {markScore} from "./redux/myAppointments/thunks";
import {styles} from "./styles/AppointmentScoringScreenStyles";

function AppointmentScoringScreen(){
	const isDarkMode = useColorScheme() === "dark";
	const navigation = useNavigation();
	const [score,setScore] = useState<number>(0);
	const route = useRoute<RouteProp<ParamList,"Detail">>();
	const completedUnScore = useSelector((state:IRootState)=>state.myAppointmentsPage.completedUnScoredSchedule);
	const dispatch = useDispatch();
	// console.log(route)

	return(
		<View style={[styles.pageContainer,{backgroundColor:Colors.white}]}>
			<View style={styles.imageContainer}>
				{route.params && <Image source={{uri:`${envfile.REACT_APP_IMAGE_PATH}/treatments/${completedUnScore.
				filter(schedule=>schedule.id === route.params.appointmentId).map(schedule=>schedule.image)}`}}
				style={styles.image}/>}
			</View>
			<View style={styles.titleRow}>
				<View>
					<View style={styles.titleMinorRow}>
						<Text style={styles.titleMinor}>{completedUnScore.filter(schedule=>schedule.id
						=== route.params.appointmentId).map(schedule=>schedule.timeslot)}</Text>
						<Text style={styles.titleMinor}>{completedUnScore.filter(schedule=>schedule.id
						=== route.params.appointmentId).map(schedule=>schedule.name)}</Text>
					</View>
						<Text style={styles.title}>{completedUnScore.filter(schedule=>schedule.id
						=== route.params.appointmentId).map(schedule=>schedule.title)}</Text>
				</View>
				<TouchableOpacity style={styles.resetContainer} onPress={() => {setScore(0)}}>
					<Text style={styles.reset}>重設</Text>
				</TouchableOpacity>
			</View>
			<View style={styles.scoreContainer}>
				<TouchableOpacity style={styles.scoreButton} onPress={() => {
					setScore(1);
				}}>
					{score && score >= 1 ? <Icon name="star" size={24} color={"#ff5500"}></Icon> :
					<Icon name="star" size={24} color={"#cccccc"}></Icon>
					}
				</TouchableOpacity>
				<TouchableOpacity style={styles.scoreButton} onPress={() => {
					setScore(2);
				}}>
					{score && score >= 2 ? <Icon name="star" size={24} color={"#ff5500"}></Icon> :
					<Icon name="star" size={24} color={"#cccccc"}></Icon>
					}
				</TouchableOpacity>
				<TouchableOpacity style={styles.scoreButton} onPress={() => {
					setScore(3);
				}}>
					{score && score >= 3 ? <Icon name="star" size={24} color={"#ff5500"}></Icon> :
					<Icon name="star" size={24} color={"#cccccc"}></Icon>
					}
				</TouchableOpacity>
				<TouchableOpacity style={styles.scoreButton} onPress={() => {
					setScore(4);
				}}>
					{score && score >= 4 ? <Icon name="star" size={24} color={"#ff5500"}></Icon> :
					<Icon name="star" size={24} color={"#cccccc"}></Icon>
					}
				</TouchableOpacity>
				<TouchableOpacity style={styles.scoreButton} onPress={() => {
					setScore(5);
				}}>
					{score && score >= 5 ? <Icon name="star" size={24} color={"#ff5500"}></Icon> :
					<Icon name="star" size={24} color={"#cccccc"}></Icon>
					}
				</TouchableOpacity>
			</View>
			<TouchableOpacity style={styles.confirmButton} onPress={() => {
				dispatch(markScore(route.params.appointmentId,score))
				navigation.navigate("FinishScoringScreen");
			}}>
				<Text style={styles.confirmText}>確認</Text>
			</TouchableOpacity>
			<TouchableOpacity style={styles.backButton} onPress={() => {
				navigation.navigate("MyAppointmentsScreen");
			}}>
				<Text style={styles.backText}>返回</Text>
			</TouchableOpacity>
		</View>
	)
}

export default AppointmentScoringScreen;