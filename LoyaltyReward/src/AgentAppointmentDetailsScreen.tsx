import {RouteProp,useNavigation,useRoute} from '@react-navigation/native';
import React,{useState} from 'react';
import {useEffect} from "react";
import {View,Image,Text,useColorScheme,TouchableOpacity} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {useDispatch,useSelector} from "react-redux";
import envfile from "../envfile";
import {ParamList} from "../models";
import {IRootState} from "../store";
import {ContactAppointmentCarousel} from './components/CarouselSlide';
import {LongAgentAppointmentPicker} from './components/Picker';
import {sendTimeslotFail, updateCustomerId} from "./redux/agentTreatment/actions";
import {sendTimeslot} from "./redux/agentTreatment/thunks";
import {updateSearchWord} from "./redux/searchHeader/actions";
import {styles} from './styles/AgentAppointmentDetailsScreenStyles';

const time = [
	{label:"2020-07-02 14:00",value:"2020-07-02 14:00"},
	{label:"2020-07-03 15:00",value:"2020-07-03 15:00"},
	{label:"2020-07-06 16:00",value:"2020-07-06 16:00"},
]
const place = [
	{label:"荃灣總部",value:"1"},
	{label:"上環港島分店",value:"2"},
]

function AgentAppointmentDetailsScreen() {
    const isDarkMode = useColorScheme() === 'dark';
    const navigation = useNavigation();
	const contactCard = useSelector((state:IRootState)=>state.agentHomePage.contact);
	const route = useRoute<RouteProp<ParamList,"Detail">>();
	const treatment = useSelector((state:IRootState)=>state.agentTreatmentPage.treatment);
	const sendTimeslotSuccess = useSelector((state:IRootState)=>state.agentTreatmentPage.timeslotSuccess);
	const [timeId,setTimeId] = useState<string|null>(null);
	const [placeId,setPlaceId] = useState<string|null>(null);
	const customerId = useSelector((state:IRootState)=>state.agentTreatmentPage.customerId);
	const dispatch = useDispatch();
	// console.log(route.params)

	useEffect(() => {
		dispatch(updateSearchWord(""))
	},[dispatch])

    return (
        <View
            style={[
                styles.pageContainer,
                {backgroundColor:Colors.white},
            ]}>
            <View style={styles.imageContainer}>
                <Image source={{uri:`${envfile.REACT_APP_IMAGE_PATH}/treatments/${treatment.filter(appointment=>appointment.id
				=== route.params.treatmentId).map(appointment=>appointment.image)}`}} style={styles.image}/>
            </View>
			<Text style={styles.title}>{treatment.filter(appointment=>appointment.id === route.params.treatmentId)
			.map(appointment=>appointment.title)}</Text>
            <View style={styles.flexContainer}>
                <View style={styles.mainTextContainer}>
                    <Text style={styles.mainText}>聯絡客戶</Text>
                </View>
                <ContactAppointmentCarousel carouselItems={contactCard}/>
            </View>
            <View style={styles.timeContainer}>
                <Text style={styles.timeText}>日期及時間</Text>
                <LongAgentAppointmentPicker label={"選擇時間"} items={time} criteria={setTimeId}/>
            </View>
            <View style={styles.placeContainer}>
                <Text style={styles.timeText}>分店</Text>
                <LongAgentAppointmentPicker label={"選擇地點"} items={place} criteria={setPlaceId}/>
            </View>
			<View style={styles.warningContainer}>
			{sendTimeslotSuccess === false && <Text style={styles.warning}>未填寫所有資料</Text>}
			</View>
            <TouchableOpacity
                style={styles.confirmButton}
                onPress={() => {
					{(!placeId || !timeId || !customerId) && dispatch(sendTimeslotFail())}
					{placeId && timeId && customerId && dispatch(sendTimeslot(timeId,placeId,customerId,route.params.treatmentId))}
					{timeId && placeId && customerId && navigation.navigate("AgentFinishAppointmentScreen",{
						customer:customerId,
						time:timeId,
						place:(place.filter(timeslot=>timeslot.value === placeId).map(timeslot=>timeslot.label))[0],
					})}
                }}>
                <Text style={styles.confirmText}>確認</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.backButton}
                onPress={() => {
					dispatch(updateCustomerId(null))
                    navigation.navigate("AgentAvailableAppointmentsScreen");
                }}>
                <Text style={styles.backText}>返回</Text>
            </TouchableOpacity>
        </View>
    );
}

export default AgentAppointmentDetailsScreen;
