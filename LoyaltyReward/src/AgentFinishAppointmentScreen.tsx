import {RouteProp, useNavigation, useRoute} from "@react-navigation/native";
import React from "react";
import {SafeAreaView,View,Image,Text,TouchableOpacity} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import {ParamList} from "../models";
import {IRootState} from "../store";
import {updateCustomerId} from "./redux/agentTreatment/actions";
import {styles} from "./styles/AgentFinishAppointmentScreenStyles";

function AgentFinishAppointmentScreen(){
	const navigation = useNavigation();
	const dispatch = useDispatch();
	const route = useRoute<RouteProp<ParamList,"Detail">>();
	const customerCard = useSelector((state:IRootState)=>state.agentHomePage.contact);

	return(
		<SafeAreaView>
			<View style={styles.imageContainer}>
				<Image style={styles.image} source={require("../images/trophy.png")}/>
				<Text style={styles.successText}>預約成功</Text>
			</View>
			<View>
			<View style={styles.textContainer}>
					<Text style={styles.text}>客戶名稱</Text>
					<Text>{customerCard.filter((customer)=>customer.customer_id === route.params.customer)
					.map((customer)=>customer.display_name)}</Text>
				</View>
				<View style={styles.textContainer}>
					<Text style={styles.text}>預約日期</Text>
					<Text>{route.params.time.slice(0,10)}</Text>
				</View>
				<View style={styles.textContainer}>
					<Text style={styles.text}>預約時間</Text>
					<Text>{route.params.time.slice(10)}</Text>
				</View><View style={styles.textContainer}>
					<Text style={styles.text}>預約分店</Text>
					<Text>{route.params.place}</Text>
				</View>
			</View>
			<TouchableOpacity style={styles.confirmButton} onPress={() => {
				dispatch(updateCustomerId(null))
				navigation.navigate("AgentAvailableAppointmentsScreen")
			}}>
				<Text style={styles.confirmText}>返回</Text>
			</TouchableOpacity>
		</SafeAreaView>
	)
}

export default AgentFinishAppointmentScreen;