import React from 'react';
import {useState} from 'react';
import {useColorScheme,TouchableWithoutFeedback,View,Text} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {styles} from './styles/AgentSalePerformanceScreenStyles';
import BarChartGraph from "./components/BarChartGraph";
import {useEffect} from "react";
import {useDispatch,useSelector} from "react-redux";
import {getDayAppointment,getDayReferral,getMonthAppointment,getMonthReferral,getMyPerformance,getYearAppointment,
getYearReferral} from "./redux/agentSalePerformance/thunks";
import {IRootState} from "../store";

function AgentSalePerformanceScreen() {
    const isDarkMode = useColorScheme() === 'dark';
    const [isToday,setIsToday] = useState(true);
    const [isThisWeek,setIsThisWeek] = useState(false);
    const [isThisMonth,setIsThisMonth] = useState(false);
	// const myReferral = useSelector((state:IRootState)=>state.agentSalePerformancePage.referral);
	// const myAppointment = useSelector((state:IRootState)=>state.agentSalePerformancePage.appointment)
	const dayRankReferral = useSelector((state:IRootState)=>state.agentSalePerformancePage.dayRankReferral)
	// console.log(dayRankReferral)
	const monthRankReferral = useSelector((state:IRootState)=>state.agentSalePerformancePage.monthRankReferral)
	const yearRankReferral = useSelector((state:IRootState)=>state.agentSalePerformancePage.yearRankReferral)
	const dayRankAppointment = useSelector((state:IRootState)=>state.agentSalePerformancePage.dayRankAppointment)
	const monthRankAppointment = useSelector((state:IRootState)=>state.agentSalePerformancePage.monthRankAppointment)
	const yearRankAppointment = useSelector((state:IRootState)=>state.agentSalePerformancePage.yearRankAppointment)
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getMyPerformance())
		dispatch(getDayReferral())
		dispatch(getMonthReferral())
		dispatch(getYearReferral())
		dispatch(getDayAppointment())
		dispatch(getMonthAppointment())
		dispatch(getYearAppointment())
	},[dispatch])

    const dailySaleCustomerData = {
        labels:dayRankReferral.length > 0 ? dayRankReferral.map(referral=>referral.display_name) :
		["中介人1","中介人2","中介人3","中介人4","中介人5"],
        datasets:[
            {data:dayRankReferral.length > 0 ? dayRankReferral.map(referral=>parseInt(referral.referral_count)) :
			[0,0,0,0,0]},
        ],
    }
    const dailySaleTreatmentData = {
        labels:dayRankAppointment.length > 0 ? dayRankAppointment.map(referral=>referral.display_name) :
		["中介人1","中介人2","中介人3","中介人4","中介人5"],
        datasets:[
            {data:dayRankAppointment.length > 0 ? dayRankAppointment.map(referral=>parseInt(referral.appointment_count)) :
			[0,0,0,0,0]},
        ],
    }
    const monthlySaleCustomerData = {
		labels:monthRankReferral.length > 0 ? monthRankReferral.map(referral=>referral.display_name) :
		["中介人1","中介人2","中介人3","中介人4","中介人5"],
        datasets:[
            {data:monthRankReferral.length > 0 ? monthRankReferral.map(referral=>parseInt(referral.referral_count)) :
			[0,0,0,0,0]},
        ],
    }
    const monthlySaleTreatmentData = {
		labels:monthRankAppointment.length > 0 ? monthRankAppointment.map(referral=>referral.display_name) :
		["中介人1","中介人2","中介人3","中介人4","中介人5"],
        datasets:[
            {data:monthRankAppointment.length > 0 ? monthRankAppointment.map(referral=>parseInt(referral.appointment_count)) :
			[0,0,0,0,0]},
        ],
    }
	const yearlySaleCustomerData = {
        labels:yearRankReferral.length > 0 ? yearRankReferral.map(referral=>referral.display_name) :
		["中介人1","中介人2","中介人3","中介人4","中介人5"],
        datasets:[
            {data:yearRankReferral.length > 0 ? yearRankReferral.map(referral=>parseInt(referral.referral_count)) :
			[0,0,0,0,0]},
        ],
    }
    const yearlySaleTreatmentData = {
		labels:yearRankReferral.length > 0 ? yearRankAppointment.map(referral=>referral.display_name) :
		["中介人1","中介人2","中介人3","中介人4","中介人5"],
        datasets:[
            {data:yearRankReferral.length > 0 ? yearRankAppointment.map(referral=>parseInt(referral.appointment_count)) :
			[0,0,0,0,0]},
        ],
    }

	let dayTotalReferral = 0
	for(let i = 0; i < dailySaleCustomerData.datasets[0].data.length; i++){
		dayTotalReferral += dailySaleCustomerData.datasets[0].data[i]
	}
	let dayTotalAppointment = 0
	for(let i = 0; i < dailySaleTreatmentData.datasets[0].data.length; i++){
		dayTotalAppointment += dailySaleTreatmentData.datasets[0].data[i]
	}
	let monthTotalReferral = 0
	for(let i = 0; i < dailySaleCustomerData.datasets[0].data.length; i++){
		monthTotalReferral += monthlySaleCustomerData.datasets[0].data[i]
	}
	let monthTotalAppointment = 0
	for(let i = 0; i < dailySaleTreatmentData.datasets[0].data.length; i++){
		monthTotalAppointment += monthlySaleTreatmentData.datasets[0].data[i]
	}
	let yearTotalReferral = 0
	for(let i = 0; i < dailySaleCustomerData.datasets[0].data.length; i++){
		yearTotalReferral += yearlySaleCustomerData.datasets[0].data[i]
	}
	let yearTotalAppointment = 0
	for(let i = 0; i < dailySaleTreatmentData.datasets[0].data.length; i++){
		yearTotalAppointment += yearlySaleTreatmentData.datasets[0].data[i]
	}

    return (
        <View
            style={[styles.flexContainer,{backgroundColor:Colors.white}]}>
            <View style={styles.titleContainer}>
                <TouchableWithoutFeedback onPress={() => {
					setIsToday(true);
					setIsThisWeek(false);
					setIsThisMonth(false);
				}}>
                    <View style={[isToday && styles.title,styles.allTitle]}>
                        <Text style={isToday ? styles.titleFocusedText : styles.titleText}>今天</Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => {
					setIsToday(false);
					setIsThisWeek(true);
					setIsThisMonth(false);
				}}>
                    <View style={[isThisWeek && styles.title,styles.allTitle]}>
                        <Text style={isThisWeek ? styles.titleFocusedText : styles.titleText}>今個月</Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => {
					setIsToday(false);
					setIsThisWeek(false);
					setIsThisMonth(true);
				}}>
                    <View style={[isThisMonth && styles.title,styles.allTitle]}>
                        <Text style={isThisMonth ? styles.titleFocusedText : styles.titleText}>今年</Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
            {isToday && (<View style={styles.contentContainer}>
				<View style={styles.barChartList}>
					<View style={styles.barChartContainer}>
						<View style={styles.subheadRow}>
							<Text style={styles.subHead}>新客戶: {dayTotalReferral}位</Text>
							<BarChartGraph datasets={dailySaleCustomerData} yLabel="" ySuffix="位"/>
						</View>
					</View>
					<View style={styles.barChartContainer}>
						<View style={styles.subheadRow}>
							<Text style={styles.subHead}>療程: {dayTotalAppointment}次</Text>
							<BarChartGraph datasets={dailySaleTreatmentData} yLabel="" ySuffix="次"/>
						</View>
					</View>
				</View>
			</View>)}
            {isThisWeek && (<View style={styles.contentContainer}>
				<View style={styles.barChartList}>
					<View style={styles.barChartContainer}>
						<View style={styles.subheadRow}>
							<Text style={styles.subHead}>新客戶: {monthTotalReferral}位</Text>
							<BarChartGraph datasets={monthlySaleCustomerData} yLabel="" ySuffix="位"/>
						</View>
					</View>
					<View style={styles.barChartContainer}>
						<View style={styles.subheadRow}>
							<Text style={styles.subHead}>療程: {monthTotalAppointment}次</Text>
							<BarChartGraph datasets={monthlySaleTreatmentData} yLabel="" ySuffix="次"/>
						</View>
					</View>
				</View>
			</View>)}
            {isThisMonth && (<View style={styles.contentContainer}>
				<View style={styles.barChartList}>
					<View style={styles.barChartContainer}>
						<View style={styles.subheadRow}>
							<Text style={styles.subHead}>新客戶: {yearTotalReferral}位</Text>
							<BarChartGraph datasets={yearlySaleCustomerData} yLabel="" ySuffix="位"/>
						</View>
					</View>
					<View style={styles.barChartContainer}>
						<View style={styles.subheadRow}>
							<Text style={styles.subHead}>療程: {yearTotalAppointment}次</Text>
							<BarChartGraph datasets={yearlySaleTreatmentData} yLabel="" ySuffix="次"/>
						</View>
					</View>
				</View>
			</View>)}
        </View>
    )
}

export default AgentSalePerformanceScreen;