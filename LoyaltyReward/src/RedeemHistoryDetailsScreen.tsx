import {RouteProp,useRoute} from "@react-navigation/native";
import React,{useEffect, useState} from "react";
import {useColorScheme,ScrollView,View,Text,Image} from "react-native";
import QRCode from "react-native-qrcode-svg";
import {Colors} from "react-native/Libraries/NewAppScreen";
import {useDispatch,useSelector} from "react-redux";
import envfile from "../envfile";
import {OrderDetailList} from "../models";
import {IRootState} from "../store";
import {getAllPickupLocations, getOrderDetail} from "./redux/redeem/thunks";
import {styles} from "./styles/RedeemHistoryDetailsScreenStyles";

export const RewardRedeemed:React.FC<{name:string,amount:string,redeem_points:string,imageSource:string}> = ({name,amount,redeem_points,imageSource}) => {
	return (
		<View style={styles.giftContainer}>
			<View style={styles.description}>
				<Text style={styles.name}>{name}</Text>
				<Text style={styles.giftType}>數量：{amount}</Text>
				<Text>{parseInt(amount)*parseInt(redeem_points)} pt</Text>
			</View>
			<Image style={styles.image} source={{uri:`${envfile.REACT_APP_IMAGE_PATH}/rewards/${imageSource}`}}/>
		</View>
	)
}

function RedeemHistoryDetailsScreen(){
	const isDarkMode = useColorScheme() === "dark";
	const route = useRoute<RouteProp<OrderDetailList,"Detail">>();
	const ordersDetail = useSelector((state:IRootState)=>state.redeemPage.orderDetail);
	const orderIntro = useSelector((state:IRootState) => state.pointPage.pointsRedeemedHistoryRow)
	const dispatch = useDispatch();
	const placeCriteria = useSelector((state:IRootState)=>state.redeemPage.rewardPickupLocation);
	// console.log(orderIntro);

	useEffect(() => {
		dispatch(getAllPickupLocations())
	},[dispatch])

	useEffect(() => {
		dispatch(getOrderDetail(route.params.order_ref))
	},[dispatch,route.params.order_ref])

	return(
		<View style={[styles.flexContainer,{backgroundColor:Colors.white}]}>
			<View style={styles.titleContent}>
				<Text style={styles.titleText}>時間</Text>
				<Text style={styles.middleTitleText}>換領編號</Text>
				<View style={styles.finalTitleTextContainer}>
					<Text style={styles.titleText}>使用積分</Text>
				</View>
			</View>
			<View style={styles.content}>
				<Text style={styles.yearText}>{(orderIntro.filter(orderDetail=>orderDetail.order_ref === route.params.order_ref)
				.map(orderDetail=>orderDetail.date))[0].slice(0,16)}</Text>
				<Text style={styles.middleText}>{route.params.order_ref}</Text>
				<View style={styles.finalText}>
					<Text>-{(orderIntro.filter(orderDetail=>orderDetail.order_ref === route.params.order_ref)
					.map(orderDetail=>orderDetail.points))[0]} pt</Text>
				</View>
			</View>
			<View>
				<ScrollView style={styles.topContainer}>
					{ordersDetail?.redeem_items.map((gift,index)=>(
						<RewardRedeemed key={index} name={gift.name} amount={gift.amount}
						redeem_points={gift.redeem_points} imageSource={gift.image}/>
					))}
				</ScrollView>
			</View>
			<View style={styles.qrcodeContainer}>
				<QRCode value={`/QR/orderRef/${(placeCriteria.filter(criteria=>criteria.name === ordersDetail?.branch.location)
				.map(criteria=>criteria.id))[0]}/${route.params.order_ref}`}
					// logo={require("../images/badge.png")} logoSize={100}
				size={80}
				></QRCode>
			</View>
			<View style={styles.payContainer}>
				<Text style={styles.payTitle}>支付積分</Text>
				<View style={styles.payRowContainer}>
					<Text>總共付出積分</Text>
					<Text>{ordersDetail?.branch.points} pt</Text>
				</View>
			</View>
			<View style={styles.placeContainer}>
				<Text style={styles.payTitle}>兌換資料</Text>
				<View style={styles.payRowContainer}>
					<Text>地點</Text>
					<Text>{ordersDetail?.branch.location}</Text>
				</View>
			</View>
		</View>
	)
}

export default RedeemHistoryDetailsScreen;
