import {FontAwesomeIcon} from "@fortawesome/react-native-fontawesome";
import React,{useEffect,useState} from "react";
import {View,Image,Text,useColorScheme,TouchableOpacity,FlatList} from "react-native";
import {Colors} from "react-native/Libraries/NewAppScreen";
import {styles} from "./styles/AgentAvailableAppointmentsScreenStyles";
import {AgentAvailableTreatment} from "../models";
import {useNavigation} from "@react-navigation/native";
import {MiddleCriteriaPicker} from "./components/Picker";
import {useDispatch,useSelector} from "react-redux";
import {getAllTreatments,placeCriteria,typeCriteria,getTreatments} from "./redux/agentTreatment/thunks";
import {IRootState} from "../store";
import envfile from "../envfile";

export const Appointment:React.FC<{imageSource:string,id:number}> = ({imageSource,children,id}) => {
	const navigation = useNavigation();
	return(
		<View style={styles.appointment}>
			<View style={styles.appointmentImageContainer}>
				<Image source={{uri:`${envfile.REACT_APP_IMAGE_PATH}/treatments/${imageSource}`}}
				style={styles.appointmentImage}/>
			</View>
			<TouchableOpacity style={styles.appointmentTextContainer} onPress={() => {
				navigation.navigate("AgentAppointmentDetailsScreen",{
					treatmentId:id
				})
			}}>
				<Text style={styles.appointmentText}>{children}</Text>
			</TouchableOpacity>
		</View>
	)
}

function AgentAvailableAppointmentsScreen(){
	const isDarkMode = useColorScheme() === "dark";
	const [filter,setFilter] = useState(false);
	const treatment = useSelector((state:IRootState)=>state.agentTreatmentPage.treatment)
	const typeCriterion = useSelector((state:IRootState)=>state.agentTreatmentPage.type);
	const placeCriterion = useSelector((state:IRootState)=>state.agentTreatmentPage.place);
	const dispatch = useDispatch();
	const bodyArea = typeCriterion.map((criterion) => ({label:criterion.service,value:criterion.id + ""}))
	const place = placeCriterion.map((criterion) => ({label:criterion.name,value:criterion.id + ""}))
	const treatmentsLength = treatment.length
	const [typeId,setTypeId] = useState<number|null>(null);
	const [placeId,setPlaceId] = useState<number|null>(null);
	useEffect(() => {
		dispatch(getAllTreatments());
		dispatch(typeCriteria());
		dispatch(placeCriteria());
	},[dispatch])

	useEffect(() => {
		dispatch(getTreatments(typeId,placeId));
	},[dispatch,typeId,placeId])

	return(
		<View style={[(filter ? styles.flexContainer : styles.flex),{backgroundColor:Colors.white}]}>
			<View style={styles.titleContainer}>
				<View>
					<Text>{treatmentsLength}項</Text>
				</View>
				<View style={styles.selectedButtonContainer}>
					<TouchableOpacity style={styles.arrangeButton} onPress={() => {
						setFilter(!filter)
						setTypeId(null)
						setPlaceId(null)
						dispatch(getAllTreatments());
					}}>
						{filter && <FontAwesomeIcon icon="filter" size={12}></FontAwesomeIcon>}
						<View style={styles.filter}>
							<Text style={filter ? styles.arrangeNow : styles.notArrange}>篩選</Text>
						</View>
					</TouchableOpacity>
				</View>
			</View>
			{filter && <View style={styles.filterContainer}>
				<View style={styles.pickerContainer}>
					<Text style={styles.pickerTitle}>類型</Text>
					<MiddleCriteriaPicker defaultValue="" label={"選擇類型"} items={bodyArea} criteria={setTypeId}
					reactDispatch={getAllTreatments()} nearCriteria={placeId}/>
				</View>
				<View style={styles.pickerContainer}>
					<Text style={styles.pickerTitle}>地點</Text>
					<MiddleCriteriaPicker defaultValue="" label={"選擇地點"} items={place} criteria={setPlaceId}
					reactDispatch={getAllTreatments()} nearCriteria={typeId}/>
				</View>
			</View>}
			<View style={filter ? styles.longScrollView : styles.scrollView}>
				<FlatList<AgentAvailableTreatment> data={treatment} renderItem={(row)=>(
					<Appointment imageSource={row.item.image} id={row.item.id} key={row.item.id}>{row.item.title}</Appointment>
				)}/>
			</View>
		</View>
	)
}

export default AgentAvailableAppointmentsScreen;