import {useNavigation,StackActions} from "@react-navigation/native";
import React,{useState,useEffect,useCallback} from "react";
import {ScrollView,View,TextInput,Text,TouchableOpacity,TouchableWithoutFeedback,Keyboard,KeyboardAvoidingView,
Platform,ImageBackground,SafeAreaView} from "react-native";
import {useDispatch,useSelector} from "react-redux";
import {loginNull} from "./redux/agentAuth/thunks";
import {resetNull} from "./redux/agentUser/thunks";
import {IRootState} from "../store";
import {backgroundStyles,flexStyles} from "./styles/LoadingScreenStyles";
import {styles} from "./styles/AgentLoginScreenStyles";
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome'
import {authorize,prefetchConfiguration} from "react-native-app-auth";
import {loginGoogleSuccess} from "./redux/socialAuth/actions";
import {configs} from "../auth.config"
import {handleAgentFacebookAuthorize, handleAgentGoogleAuthorize, handleAppleAuthorize} from "./redux/agentSocialAuth/thunks";
import {AgentRoleCheckBox} from "./components/CheckBox";
import {loginAgent} from "./redux/agentAuth/thunks";
import {LoginButton,AccessToken,Profile} from 'react-native-fbsdk-next';
import appleAuth,{AppleButton} from "@invertase/react-native-apple-authentication";
import envfile from "../envfile";

function AgentLoginScreen(){
	const navigation = useNavigation();
	const isLoggedIn = useSelector((state:IRootState)=>state.agentAuth.isLoggedIn);
	const dispatch = useDispatch();
	const [username,setUsername] = useState("");
	const [password,setPassword] = useState("");
	const isAuthenticatedAgent = useSelector((state:IRootState)=>state.agentAuth.isAuthenticated);
	const [keyboardShow,setKeyboardShow] = useState(false);

	const handleAuthorize = useCallback(
		async(provider:any) => {
			const config = configs[provider];
			const newAuthState = await authorize(config);
			if(provider === "google"){
				dispatch(handleAgentGoogleAuthorize(newAuthState.accessToken))
				dispatch(loginGoogleSuccess(newAuthState))
			}
		}
	,[dispatch]);

	useEffect(() => {
		if(isAuthenticatedAgent){
			navigation.dispatch(
				StackActions.replace("AgentMain")
			)
		}
	},[isAuthenticatedAgent])

	useEffect(() => {
		prefetchConfiguration({
			warmAndPrefetchChrome:true,
			...configs.google
		})
	},[])

	async function onAppleButtonPress() {
		// performs login request
		const appleAuthRequestResponse = await appleAuth.performRequest({
			requestedOperation:appleAuth.Operation.LOGIN,
			requestedScopes:[appleAuth.Scope.EMAIL,appleAuth.Scope.FULL_NAME],
		})

		// get current authentication state for user
		// This method must be tested on a real device. On the iOS simulator it always throws an error.
		const credentialState = await appleAuth.getCredentialStateForUser(appleAuthRequestResponse.user);

		// use credentialState response to ensure the user is authenticated
		if(credentialState === appleAuth.State.AUTHORIZED){
			// console.log(appleAuthRequestResponse)
			// console.log(appleAuth)
		  	// user is authenticated
			dispatch(handleAppleAuthorize(`${envfile.REACT_APP_APPLE_APP_ID}`,appleAuthRequestResponse.identityToken,appleAuthRequestResponse.authorizationCode))
		}
	}

	return(
		<KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"} style={flexStyles.flex}>
			<TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false} style={{flex:1}}>
				<ScrollView scrollEnabled={keyboardShow} style={styles.main}>
					<ImageBackground source={require("../images/agentBackground.png")} style={backgroundStyles.image}>
						<SafeAreaView style={flexStyles.flex}>
							<AgentRoleCheckBox/>
							<View style={styles.login}>
								<View style={styles.loginSub}>
									<Text style={styles.title}>登入</Text>
									<Text>用戶名</Text>
									<TextInput onFocus = {() => setKeyboardShow(true)} style={styles.textInput} placeholder="username" value={username}
									onChangeText={setUsername} onBlur = {() => {
										setKeyboardShow(false)
									}}></TextInput>
									<Text>密碼</Text>
									<TextInput onFocus = {() => setKeyboardShow(true)} style={styles.textInput} placeholder="password" value={password}
									onChangeText={setPassword}
									secureTextEntry onBlur = {() => {
										setKeyboardShow(false)
									}}></TextInput>
									<TouchableOpacity onPress={()=>{
										navigation.navigate("ResetPassword");
										dispatch(loginNull());
										dispatch(resetNull());
									}}>
										<Text style={styles.resetButton}>忘記密碼</Text>
									</TouchableOpacity>
									<LoginButton
									// style={styles.socialFacebookLogin}
										onLoginFinished={async (error,result) => {
											if(!result.isCancelled)
											// {
											// 	console.log("login is cancelled");
											// }else
											{
												const token = await AccessToken.getCurrentAccessToken()
												const customerProfile = await Profile.getCurrentProfile()
												dispatch(handleAgentFacebookAuthorize(token?.accessToken,customerProfile?.email))
											}

										}}
										onLogoutFinished={()=>{
											dispatch(loginNull())
										}}
									/>
									<View style={styles.socialLoginContainer}>
										<TouchableOpacity onPress={()=>{
											handleAuthorize("google")
										}}>
											<View style={styles.socialLogin}>
												<FontAwesomeIcon color={"#555555"} size={18} icon={["fab","google"]}/>
											</View>
										</TouchableOpacity>
										<TouchableOpacity>
										<AppleButton
											buttonStyle={AppleButton.Style.BLACK}
											buttonType={AppleButton.Type.SIGN_IN}
											style={styles.appleButton}
											onPress={onAppleButtonPress}
										/>
									</TouchableOpacity>
									</View>
								</View>
							</View>
							<View style={styles.notificationContainer}>
								{isLoggedIn === false && (
									<View style={styles.dangerNotification}>
										<Text style={styles.dangerNotificationText}>用戶名/密碼錯誤</Text>
									</View>
								)}
							</View>
							<View style={styles.loginContainer}>
								<TouchableOpacity onPress={()=>{
									dispatch(loginAgent(username,password))
								}}>
									<View style={styles.loginButton}>
										<Text style={styles.button}>登入</Text>
									</View>
								</TouchableOpacity>
							</View>
						</SafeAreaView>
					</ImageBackground>
				</ScrollView>
			</TouchableWithoutFeedback>
		</KeyboardAvoidingView>
	)
}

export default AgentLoginScreen;