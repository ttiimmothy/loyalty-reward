/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import {useNavigation,StackActions} from "@react-navigation/native";
import React,{useEffect} from 'react';
import {SafeAreaView,StatusBar,Text,useColorScheme,View} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {useDispatch, useSelector} from "react-redux";
import {CarouselSlide,ContactCarousel} from "./components/CarouselSlide";
import {IRootState} from "../store";
import {styles} from "./styles/AgentHomeScreenStyles";
import {getAdvertisement, getContactCard, myPerformance} from "./redux/agentHome/thunks";

const AgentHomeScreen = () => {
	const isDarkMode = useColorScheme() === "dark";
	const advertisementCard = useSelector((state:IRootState)=>state.agentHomePage.advertisement);
	const navigation = useNavigation();
	const isAuthenticatedAgent = useSelector((state:IRootState)=>state.agentAuth.isAuthenticated);
	const contactCard = useSelector((state:IRootState)=>state.agentHomePage.contact);
	const salePerformance = useSelector((state:IRootState)=>state.agentHomePage.performance);
	// console.log(contactCard);
	// console.log(salePerformance)
	const dispatch = useDispatch();
	useEffect(() => {
		if(!isAuthenticatedAgent){
			navigation.dispatch(
				StackActions.replace("Login")
			)
		}
	},[isAuthenticatedAgent])
	useEffect(() => {
		dispatch(getAdvertisement());
		dispatch(getContactCard());
		dispatch(myPerformance())
	},[dispatch])

	return (
		<SafeAreaView style={[styles.flexTopContainer,{backgroundColor:Colors.white}]}>
			<StatusBar barStyle={isDarkMode ? "light-content" : "dark-content"}/>
			{/* <ScrollView contentInsetAdjustmentBehavior="automatic" style={[styles.flexTopContainer,backgroundStyle]}> */}
				<CarouselSlide carouselItems={advertisementCard}/>
				<View style={{backgroundColor: isDarkMode ? Colors.black : Colors.white}}>
					<View style={styles.flexContainer}>
						<View style={styles.mainTextContainer}><Text style={styles.mainText}>聯絡客戶</Text></View>
						<ContactCarousel carouselItems={contactCard}/>
						<View style={styles.mainSellTextContainer}><Text style={styles.mainText}>本月銷售</Text></View>
						<View style={styles.serviceContainer}>
							<Text style={styles.customerText}>新客戶: {salePerformance.referral_count}位</Text>
							<Text style={styles.customerText}>療程: {salePerformance.appointment_count}</Text>
						</View>
					</View>
				</View>
			{/* </ScrollView> */}
		</SafeAreaView>
	);
};

export default AgentHomeScreen;
