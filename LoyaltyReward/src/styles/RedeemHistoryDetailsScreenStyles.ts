import {Dimensions,StyleSheet} from "react-native";

export const styles = StyleSheet.create({
	flexContainer:{
		height:Dimensions.get("window").height
	},
	content:{
		flexDirection:"row",
		marginHorizontal:20,
		marginBottom:10,
		justifyContent:"space-between"
	},
	middleText:{
		width:Dimensions.get("window").width * 0.42,
	},
	finalText:{
		width:55,
		alignItems:"flex-end"
	},
	topContainer:{
		height:Dimensions.get("window").height * 0.4,
		borderBottomWidth:1,
		borderColor:"#eeeeee"
	},
	giftContainer:{
		flexDirection:"row",
		justifyContent:"space-between",
		marginHorizontal:25,
		marginTop:10
	},
	giftOtherContainer:{
		flexDirection:"row",
		justifyContent:"space-between",
		marginHorizontal:25,
		marginTop:15
	},
	image:{
		height:100,
		width:100,
		borderRadius:4
	},
	description:{
		marginVertical:10,
		justifyContent:"center",
	},
	name:{
		width:Dimensions.get("window").width * 0.55,
		fontSize:20,
		fontWeight:"800",
		marginBottom:10
	},
	giftType:{
		color:"#adadad",
		fontSize:16,
		fontWeight:"600",
		marginBottom:10
	},
	payContainer:{
		paddingVertical:10,
		paddingHorizontal:20,
		borderBottomWidth:1,
		borderColor:"#eeeeee"
	},
	payTitle:{
		fontSize:20,
		color:"#adadad",
		marginBottom:10
	},
	payRowContainer:{
		flexDirection:"row",
		justifyContent:"space-between"
	},
	placeContainer:{
		paddingVertical:10,
		paddingHorizontal:20,
	},
	titleText:{
		width:Dimensions.get("window").width * 0.18,
		fontSize:20,
		fontWeight:"600",
		color:"#666666"
	},
	titleContent:{
		flexDirection:"row",
		justifyContent:"space-between",
		marginVertical:10,
		marginHorizontal:20
	},
	middleTitleText:{
		width:Dimensions.get("window").width * 0.3,
		fontWeight:"600",
		fontSize:20,
		color:"#666666"
	},
	finalTitleTextContainer:{
		width:65,
		// alignItems:"flex-end"
	},
	yearText:{
		width:Dimensions.get("window").width * 0.28,
	},
	qrcodeContainer:{
		marginTop:10,
		justifyContent:"center",
		alignItems:"center",
		// marginBottom:10
	},
})