import {StyleSheet,Dimensions} from "react-native";
import {notificationStyles} from "./LoginScreenStyles";
// const windowDimension = useWindowDimensions();

export const styles = StyleSheet.create({
	main:{
		flexDirection:"column",
		flex:1,
		// backgroundColor:"#aaff00"
	},
	title:{
		fontSize:30,
		fontWeight:"600",
		marginBottom:15
	},
	login:{
		height:Dimensions.get("window").height * 0.66,
		justifyContent:"flex-end"

	},
	loginSub:{
		marginLeft:30,
		marginTop:10,
		// flex:1
	},
	textInput:{
		marginTop:3,
		marginBottom:10,
		fontSize:20,
		borderWidth:1,
		borderColor:"#adadad",
		width:300,
		height:40,
		borderRadius:5
	},
	socialLoginContainer:{
		marginTop:10,
		flexDirection:"row"
	},
	socialLogin:{
		backgroundColor:"#eeeeee",
		height:36,
		width:36,
		borderRadius:3,
		justifyContent:"center",
		alignItems:"center",
		marginRight:5
	},
	notificationContainer:{
		marginTop:20,
		height:40,
		marginBottom:45,
		// backgroundColor:"#ff0000"
	},
	dangerNotification:{
		marginLeft:30,
		marginRight:30,
		...notificationStyles.dangerColor,
		...notificationStyles.commonNotification
	},
	dangerNotificationText:{
		fontSize:20,
		color:"#ffffff",
		fontWeight:"600"
	},
	loginContainer:{
		justifyContent:"flex-end",
		alignItems:"flex-end",
		flexDirection:"row",
		margin:30,
		// backgroundColor:"#ffcc00"
		// flex:0
	},
	resetButton:{
		marginBottom:10,
		fontSize:14,
		fontWeight:"600",
	},
	registerButton:{
		fontSize:14,
		fontWeight:"600",
		color:"#ffffff"
	},
	loginButton:{
		...notificationStyles.button
	},
	button:{
		...notificationStyles.buttonText
	},
	appleButton: {
		width:36,
		height:36,
		shadowColor:"#555555",
		shadowOpacity:0.5,
		shadowOffset:{
			width:0,
			height:3
		},
		// marginVertical:15,
	}
})