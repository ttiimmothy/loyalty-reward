import {Dimensions,StyleSheet} from "react-native";

export const styles = StyleSheet.create({
	flexContainer:{
		height:Dimensions.get("window").height
	},
	historyContainer:{
		alignItems:"center",
	},
	historyBorder:{
		width:Dimensions.get("window").width * 0.9,
		borderWidth:2,
		borderColor:"#dddddd",
		height:Dimensions.get("window").height * 0.5,
		marginTop:10,
		borderRadius:6,
		// overflow:"scroll",
		padding:10
	},
	titleText:{
		width:Dimensions.get("window").width * 0.18,
		fontSize:20,
		fontWeight:"600",
		color:"#666666"
	},
	titleContent:{
		flexDirection:"row",
		justifyContent:"space-between",
		marginBottom:10,
	},
	middleTitleText:{
		width:Dimensions.get("window").width * 0.2,
		fontWeight:"600",
		fontSize:20,
		color:"#666666"
	},
	finalTitleTextContainer:{
		width:65,
		// alignItems:"flex-end"
	},
	content:{
		flexDirection:"row",
		marginBottom:3,
		justifyContent:"space-between"
	},
	middleText:{
		width:Dimensions.get("window").width * 0.35,
		alignItems:"flex-start"
	},
	finalText:{
		width:45,
		alignItems:"flex-end"
	},
	year:{

	}
})