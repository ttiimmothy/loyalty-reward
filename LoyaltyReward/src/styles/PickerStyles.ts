import {StyleSheet} from "react-native";

export const pickerSelectStyles = StyleSheet.create({
	inputIOS:{
		fontSize:16,
		marginRight:5,
		height:30,
		width:70,
		paddingLeft:5,
		borderWidth:1,
		borderColor:"#adadad",
		borderRadius:4,
		color:"#000000",
		paddingRight:30,// to ensure the text is never behind the icon
	},
	inputAndroid:{
		fontSize:14,
		paddingHorizontal:2,
		paddingVertical:0,
		paddingLeft:5,
		borderWidth:1,
		borderColor:"#adadad",
		borderRadius:4,
		color:"#000000",
		paddingRight:30,
	},
	iconContainer:{
		top:6,
		right:8,
	}
})

export const middlePickerSelectStyles = StyleSheet.create({
	inputIOS:{
		fontSize:16,
		marginRight:5,
		height:30,
		width:110,
		paddingLeft:5,
		borderWidth:1,
		borderColor:"#adadad",
		borderRadius:4,
		color:"#000000",
		paddingRight:30,// to ensure the text is never behind the icon
	},
	inputAndroid:{
		fontSize:14,
		width:110,
		paddingVertical:0,
		paddingLeft:5,
		borderWidth:1,
		borderColor:"#adadad",
		borderRadius:4,
		color:"#000000",
		paddingRight:30,
	},
	iconContainer:{
		top:6,
		right:11,
	}
})

export const middlePickerSelectGenderStyles = StyleSheet.create({
	inputIOS:{
		fontSize:16,
		height:30,
		width:100,
		paddingLeft:5,
		borderWidth:1,
		borderColor:"#adadad",
		color:"#000000",
		paddingRight:30,// to ensure the text is never behind the icon
	},
	inputAndroid:{
		fontSize:14,
		width:100,
		paddingVertical:0,
		paddingLeft:5,
		borderWidth:1,
		borderColor:"#adadad",
		borderRadius:4,
		color:"#000000",
		paddingRight:30,
	},
	iconContainer:{
		top:6,
		right:6,
	}
})

export const longPickerSelectStyles = StyleSheet.create({
	inputIOS:{
		fontSize:16,
		marginRight:5,
		height:30,
		width:170,
		paddingLeft:5,
		borderWidth:1,
		borderColor:"#adadad",
		borderRadius:4,
		color:"#000000",
		paddingRight:30,// to ensure the text is never behind the icon
	},
	inputAndroid:{
		fontSize:14,
		width:150,
		paddingVertical:0,
		paddingLeft:5,
		borderWidth:1,
		borderColor:"#adadad",
		borderRadius:4,
		color:"#000000",
		paddingRight:30,
	},
	iconContainer:{
		top:6,
		right:12,
	}
})