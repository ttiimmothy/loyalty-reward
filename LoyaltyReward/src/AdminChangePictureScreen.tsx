import {useNavigation} from "@react-navigation/native";
import React,{useState} from "react";
import {useColorScheme,Text,View,TouchableOpacity,Image,GestureResponderEvent} from "react-native";
import {Colors} from "react-native/Libraries/NewAppScreen";
import {styles} from "./styles/AdminChangePictureScreenStyles";
import {ImagePickerResponse,launchImageLibrary,Asset} from "react-native-image-picker";
import {useDispatch} from "react-redux";
import {uploadPhoto} from "./redux/admin/thunks";

function AdminChangePictureScreen(){
	const isDarkMode = useColorScheme() === "dark";
	const navigation = useNavigation();
	const [imageSource,setImageSource] = useState<Asset[]|null>(null);
	// console.log(imageSource)
	const dispatch = useDispatch();
	const options = {
		maxHeight:800,
		maxWidth:800,
		selectionLimit:1,
		mediaType:"mixed",
		includeBase64:false,
	}
	return(
		<View style={[styles.flexContainer,{backgroundColor:Colors.white}]}>
		<View style={styles.imageContainer}>
			<Image source={imageSource && imageSource} style={styles.image}/>
		</View>
		<TouchableOpacity style={styles.addButton} onPress={(event:GestureResponderEvent)=>{
			// console.log(event.nativeEvent.touches);
			launchImageLibrary(options,(response:ImagePickerResponse)=>{
				// console.log(response.assets[0].uri)
				if(response.assets){
					// console.log(response)
					setImageSource(response.assets);
					{imageSource && dispatch(uploadPhoto(imageSource))}
				}
			})

		}}>
			<Text style={styles.addText}>新增</Text>
		</TouchableOpacity>
		<TouchableOpacity style={styles.backButton} onPress={() => {
			navigation.navigate("HomeScreen")
		}}>
			<Text style={styles.backText}>返回</Text>
		</TouchableOpacity>
		</View>
	)
}

export default AdminChangePictureScreen;