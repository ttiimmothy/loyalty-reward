import AsyncStorage from "@react-native-community/async-storage";
import envfile from "../../envfile";



export async function authFetch(url:string){
	const token = await AsyncStorage.getItem("token");
		const res = await fetch(url,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();

}