import AsyncStorage from "@react-native-community/async-storage";
import {Dispatch} from "redux";
import envfile from "../../../envfile";
import {IAuthActions,loginSuccess,loginFailure,logoutSuccess,loginNullSuccess} from "./actions";

export function loginAgent(username:string,password:string){
	return async(dispatch:Dispatch<IAuthActions>) => {
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/users/login/agents`,{
			method:"post",
			headers:{
				"Content-Type":"application/json",
			},
			body:JSON.stringify({username:username,password:password})
		})
		const result = await res.json();
		// console.log(result)
		if(result.success){
			dispatch(loginSuccess(result.id,result.data));
			await AsyncStorage.setItem("token",result.data);
		}else{
			dispatch(loginFailure(result.message));
		}
	}
}

export function userLoginAgent(username:string,password:string){
	return async(dispatch:Dispatch<IAuthActions>) => {
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/users/userLogin`,{
			method:"post",
			headers:{
				"Content-Type":"application/json",
			},
			body:JSON.stringify({username:username,password:password,role:"agent"})
		})
		const result = await res.json();
		// console.log(result)
		if(result.success){
			dispatch(loginSuccess(result.id,result.data));
			await AsyncStorage.setItem("token",result.data);
		}else{
			dispatch(loginFailure(result.message));
		}
	}
}

export function loginNull(){
	return async (dispatch:Dispatch<IAuthActions>)=>{
        dispatch(loginNullSuccess());
    }
}

export function agentLogout(){
	return async (dispatch:Dispatch<IAuthActions>)=>{
        dispatch(logoutSuccess());
        await AsyncStorage.removeItem("token");
		await AsyncStorage.removeItem("agentToken")
    }
}

export function checkAgentUser(){
	return async(dispatch:Dispatch<IAuthActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token);
		if(!token){
			return dispatch(logoutSuccess());
		}
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/users/currentUsersCheck`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result);
		if(result.success && result.lastLogin === "agents"){
			dispatch(loginSuccess(result.agent_id,token));
		}else{
			dispatch(logoutSuccess());
		}
	}
}