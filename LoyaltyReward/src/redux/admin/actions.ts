export function uploadPhotoSuccess(){
	return{
		type:"@@admin/Upload_photo" as const
	}
}


// No need action for each failed case
type Failed = "Upload_photo_failed"
export function failed(type:Failed,message:string){
	return{
		type,
		message
	}
}

export type IAdminActions = ReturnType<typeof uploadPhotoSuccess|typeof failed>