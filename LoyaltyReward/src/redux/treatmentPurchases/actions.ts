import {PlaceCriteria, Treatment,TypeCriteria} from "../../../models";

export function getAllTreatmentsSuccess(treatment:Treatment[]){
	return{
		type:"@@treatmentPurchases/Get_all_treatments" as const,
		treatment
	}
}

export function typeCriteriaSuccess(criteria:TypeCriteria[]){
	return{
		type:"@@treatmentPurchases/Get_type_criteria" as const,
		criteria
	}
}

export function placeCriteriaSuccess(criteria:PlaceCriteria[]){
	return{
		type:"@@treatmentPurchases/Get_place_criteria" as const,
		criteria
	}
}

export function getTreatmentsSuccess(treatment:Treatment[]){
	return{
		type:"@@treatmentPurchases/Get_treatments" as const,
		treatment
	}
}

export function confirmPurchasesSuccess(){
	return{
		type:"@@treatmentPurchases/Confirm_purchase" as const,
	}
}

export function setNumberFail(){
	return{
		type:"@@treatmentPurchases/Set_purchase_fail" as const
	}
}

export function setPurchaseNull(){
	return{
		type:"@@treatmentPurchases/Set_purchase_null" as const
	}
}

type Failed = "Confirm_purchase_failed"|"Get_all_treatments_failed"|"Get_type_criteria_failed"|"Get_place_criteria_failed"|
"Get_treatments_failed"|"Add_pending_schedule_failed";
export function failed(type:Failed,message:string){
	return{
		type,
		message
	}
}

export type ITreatmentPurchasesActions = ReturnType<typeof getAllTreatmentsSuccess|typeof typeCriteriaSuccess|
typeof getTreatmentsSuccess|typeof placeCriteriaSuccess|typeof getTreatmentsSuccess|typeof confirmPurchasesSuccess|
typeof setNumberFail|typeof setPurchaseNull|typeof failed>