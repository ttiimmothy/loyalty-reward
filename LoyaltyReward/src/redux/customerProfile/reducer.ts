import {UserProfile} from "../../../models";
import {ICustomerProfileActions} from "./actions"

export interface ICustomerProfileState{
    id:number;
	date_of_birth:string;
	display_name:string;
	email:string;
	mobile:string;
	username:string;
	gender:string
	update:boolean
}

const initialState = {
	id:0,
	date_of_birth:"",
	display_name:"",
	email:"",
	mobile:"",
	username:"",
	gender:"",
	update:true
}

const customerProfileReducer = (state:ICustomerProfileState = initialState,action:ICustomerProfileActions):ICustomerProfileState => {
    switch(action.type){
        case "@@customerProfile/Get_user_profile":
            return{
                ...state,
                id:action.userProfile.id,
				date_of_birth:(action.userProfile.date_of_birth),
				display_name:action.userProfile.display_name,
				email:action.userProfile.email,
				mobile:action.userProfile.mobile,
				username:action.userProfile.username,
				gender:action.userProfile.gender
            }
        case "@@customerProfile/Update_user_profile":
            return{
                ...state,
                username:action.username,
				email:action.email,
				mobile:action.mobile,
				date_of_birth:action.date_of_birth,
				display_name:action.display_name,
				gender:action.gender
            }
		case "@@customerProfile/Update_username":
			return{
				...state,
				username:action.username
			}
		case "@@customerProfile/Update_display_name":
			return{
				...state,
				display_name:action.display_name
			}
		case "@@customerProfile/Update_email":
			return{
				...state,
				email:action.email
			}
		case "@@customerProfile/Update_mobile":
			return{
				...state,
				mobile:action.mobile
			}
		case "@@customerProfile/Update_birthday":
			return{
				...state,
				date_of_birth:action.birthday
			}
		case "@@customerProfile/Update_gender":
			return{
				...state,
				gender:action.gender
			}
		case "@@customerProfile/Update_profile_fail":
			return{
				...state,
			}
        default:
            return state;
    }
}

export default customerProfileReducer;
