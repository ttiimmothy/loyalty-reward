import AsyncStorage from "@react-native-community/async-storage";
import {Dispatch} from "redux";
import envfile from "../../../envfile";
import {callSuccessSuccess,failed,IAgentCallListActions,callFailSuccess,getAllCallListSuccess,addNewContactSuccess,
addNewContactFail,whatsAppNewNumber} from "./actions";

export function callSuccess(phoneNumber:number){
	return async(dispatch:Dispatch<IAgentCallListActions>) => {
		const token = await AsyncStorage.getItem("token");
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/agents/contact/coldCall/${phoneNumber}/1`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(result.success){
			dispatch(callSuccessSuccess())
		}else{
			dispatch(failed("Call_success_failed",result.message))
		}
	}
}

export function callFail(phoneNumber:number){
	return async(dispatch:Dispatch<IAgentCallListActions>) => {
		const token = await AsyncStorage.getItem("token");
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/agents/contact/coldCall/${phoneNumber}/2`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(result.success){
			dispatch(callFailSuccess())
		}else{
			dispatch(failed("Call_fail_failed",result.message))
		}
	}
}

export function getAllCallList(){
	return async(dispatch:Dispatch<IAgentCallListActions>) => {
		const token = await AsyncStorage.getItem("token");
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/agents/index/contact`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(getAllCallListSuccess(result))
		}else{
			dispatch(failed("Get_all_call_list_failed",result.message))
		}
	}
}

export function addNewContact(phone:number){
	return async(dispatch:Dispatch<IAgentCallListActions>) => {
		const token = await AsyncStorage.getItem("token");
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/agents/contact/create/${phone}`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(result.success){
			dispatch(addNewContactSuccess());
			if(result.case === 4){
				// console.log(3)
				dispatch(whatsAppNewNumber())
			}
		}else{
			dispatch(failed("Add_new_contact_failed",result.message))
			dispatch(addNewContactFail())
		}
	}
}