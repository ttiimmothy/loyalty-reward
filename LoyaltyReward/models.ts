export interface AppointmentItem{
	id:number;
	image:string;
	text:string;
}
export interface imagesSourceIndex{
	[index:string]:any
}
export interface PickerItems{
	label:string;
	value:string;
}
export interface CarouselCard{
	item:any;
	index:number;
}
export interface Config{
	issuer:string;
	clientId:string;
	redirectUrl:string;
	scopes:Array<string>;
}
export interface TrialConfig{
	clientId:string,
	clientSecret:string,
	redirectUrl:string, // this can be any valid uri as long as it's the same as what you configured
	scopes:Array<string>, // https://developers.coinbase.com/docs/wallet/permissions
	serviceConfiguration:{
		authorizationEndpoint:string,
		tokenEndpoint:string,
		revocationEndpoint?:string,
	}
}
export interface ThirdPartyAuth{
	[index:string]:Config|TrialConfig;
}
export interface GiftItem{
	id:number;
	image:string;
	text:string;
	type:string;
	point:number
}
export interface PickerCriteriaItems{
	label:number;
	value:string;
}
export interface CarouselBarCard{
	item:{
		id:number,
		timeslot:string,
		treatment_id:number,
		name:string,
		title:string;
		image:string;
	};
	index:number;
}
export interface ContactCarouselCard{
	item:{
		id:number;
		customer_id:number;
		date_of_birth:string;
		display_name:string;
		gender:string;
		mobile:number;
	};
	index:number;
}
export interface CarouselCardRender{
	id:number;
	image:string;
	title:string;
	treatment_id:number;
}
export interface ContactCarouselCardRender{
	item:{
		name:string;
		age:string;
		gender:string;
	}
}
export interface Advertisement{
	id:number,
	title:string,
	image:string,
	treatment_id:number
}
export interface Surgery{
	id:number,
	title:string,
	image:string,
	no_of_appointments:number
}
export interface Service{
	id:number,
	service:string,
	image:string,
}
export interface Schedule{
	id:number,
	timeslot:string,
	treatment_id:number,
	name:string,
	title:string;
	image:string;
}
export interface PurchasedSchedule{
	id:number;
    remaining_amount:string,
    detail:string;
    image:string,
    title:string
}
export interface CompletedScoredSchedule{
	id:number,
	timeslot:string,
	treatment_id:number,
	name:string,
	title:string;
	image:string;
	score?:number;
}
export interface UserProfile{
	id:number;
	date_of_birth:string;
	display_name:string;
	email:string;
	mobile:string;
	username:string;
	gender:string;
}
export interface Treatment{
	id:number;
	image:string;
	title:string;
	gender:string;
}
export interface TypeCriteria{
	id:number;
	service:string;
}
export interface PlaceCriteria{
	id:number;
	name:string;
}
export type ParamList = {
	Detail:{
		appointmentId?:number;
		treatmentId?:number;
		time:string;
		place?:string;
		customer?:number;
		typeCriteria?:number;
		treatmentNumber?:number;
		purchasedId?:number;
		remaining?:string;
	}
}
export interface Reward{
	id:number;
	detail:string;
	name:string;
	redeem_points:number;
	type:string;
	image:string;
}
export interface RewardType{
	id:number;
	type:string;
}
export interface RewardPickupLocation{
	id:number;
	name:string;
}
export interface PointDetail{
	success:boolean;
	currentPoint:number
}
export type RewardList = {
	Detail:{
		rewardId?:number;
	}
}
export interface PointsEarnedHistoryRow{
	id:number;
	date:string;
	summary:string;
	location:string;
	points:string
}
export interface PointsRedeemedHistoryRow{
	order_ref:string;
	date:string;
	location:string;
	points:string
}
export interface ExpiredPointsHistoryRow{
	id:string;
    date:string;
    points:string;
}
export interface AgentAvailableTreatment{
	id:number;
	image:string;
	title:string
}
export interface AddToCartResult{
	success:boolean
}
export interface InStockItemDetail{
	id:string;
	latest_cart_id:string;
	name:string;
	image:string;
	redeem_points:string;
	type:string;
	remain_amount:string;
	amount_in_cart:string
}
export interface OutOfStockItemDetail{
	id:string;
	latest_cart_id:string;
	name:string;
	image:string;
	redeem_points:string;
	type:string;
	remain_amount:string;
	amount_in_cart:string
}
export interface ContactCard{
	id:number;
	customer_id:number;
	date_of_birth:string;
	display_name:string;
	gender:string;
	mobile:number;
}
export interface PerformanceCount{
	appointment_count:number;
	referral_count:number;
}
export interface OrderInfo{
	date:string;
	qr_code:string;
	location:string;
	order_ref:string;
	is_pickup:string;
	points:string;
}
export interface GiftInfo{
	image:string;
	amount:string;
	reward_type_id:string;
	name:string;
	remain_amount:string;
	order_ref:string;
	redeem_points:string;
}
export interface CallList{
	id:number;
	display_name:string;
	mobile:number;
	gender:string;
	date_of_birth:string;
}
export type OrderDetailList = {
	Detail:{
		order_ref?:string;
	}
}
export interface RedeemOrderDetails{
	redeem_items:GiftInfo[];
	branch:OrderInfo;
}
export interface KeywordResult{
	enquiry:[];
	rewards:[];
	treatments:[];
}
export interface KeywordResultAgent{
	treatments:[];
	contact:[];
}
export interface KeywordResultRewards{
	id:number;
	detail:string;
	image:string;
	name:string;
}
export interface KeywordResultTreatments{
	id:number;
	detail:string;
	image:string;
	title:string
}
export interface KeywordResultAgentTreatments{
	id:number;
	detail:string;
	image:string;
	title:string;
}
export interface KeywordResultAgentContacts{
	id:number;
	user_id:number;
	gender:string;
	date_of_birth:string;
	mobile:number;
	display_name:string;
	username:string;
}
export interface RankingReferral{
	id:number;
    display_name:string;
    referral_count:string;
}
export interface RankingAppointment{
	id:number;
    display_name:string;
    appointment_count:string;
}
export interface TreatmentWithId{
	detail:string;
    image:string;
    title:string;
    default_score:number;
}
export interface VerifySecurityCodeResult{
	success:boolean|null;
	message:string|null;
}
export interface ResendSecurityCodeResult{
	success:boolean|null;
	message:string|null;
}