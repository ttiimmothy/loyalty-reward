import {push} from "connected-react-router";
import React,{useEffect,useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button} from "reactstrap";
import {IRootState} from "./store";
import "./Advertisement.scss";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { getAllAdvertisements } from "./redux/advertisement/actions";
import {getAllTreatments} from "./redux/treatment/actions";

function Advertisement(){
	const sidebar = useSelector((state:IRootState)=>state.sidebar.sidebarToggle);
	const advertisements = useSelector((state:IRootState)=>state.advertisement.allAdvertisements);
	const [selectPage,setSelectPage] = useState("30");
	const [pageNumber,setPageNumber] = useState(1)
	const dispatch = useDispatch();
	// console.log(advertisements);

	useEffect(() => {
		dispatch(getAllAdvertisements(selectPage,pageNumber))
		dispatch(getAllTreatments("1000",1))
	},[dispatch,selectPage,pageNumber])

	return(
		<div className={sidebar ? "advertisement-container-active" : "treatment-container"}>
			<div className="advertisement-title-container">
				<div className="treatment-title">所有廣告</div>
				<div className="top-button-container">
					<div className="select-page-another">
					<Button color="secondary" className="add-button" onClick={() => {
						dispatch(push("/create/advertisement"))
					}}>新增廣告</Button>
					<Button className="arrow-button" onClick={() => {
						pageNumber > 1 && setPageNumber(pageNumber - 1)
					}}><FontAwesomeIcon icon="arrow-left" className="arrow"/></Button>
					<Button className="arrow-right" onClick={() => {
						setPageNumber(pageNumber + 1)
					}}><FontAwesomeIcon icon="arrow-right" className="arrow"/></Button>
					</div>
					<div className="select-page">
						<div>每頁顯示</div>
						<select className="selector" value={selectPage} onChange={(event) => {
							setSelectPage(event.currentTarget.value)
						}}>
							<option value="30">30</option>
							<option value="50">50</option>
							<option value="100">100</option>
							<option value="300">300</option>
						</select>
						<div>項</div>
						<div className="page-number">第{pageNumber}頁</div>
					</div>
				</div>
			</div>
			<div className="flex-advertisement-container">
			{advertisements.filter(advertisement=>advertisement).map((advertisement,index)=>{
			/* {advertisements.filter(advertisement=>advertisement.is_hidden === "false").map((advertisement,index)=>{ */
				return(
					<div key={index} className="content-advertisement-container"
					style={{backgroundImage:`url(${process.env.REACT_APP_IMAGE_PATH}/advertisements/${advertisement.image})`}}>
						<div className="editor">
							<div className="advertisement-content-title">{advertisement.title}</div>
							<Button color="warning" onClick={() => {
								dispatch(push(`/advertisement/${index + 1}`))
							}}>編輯廣告</Button>
						</div>
					</div>
				)
			})}
			</div>
		</div>
	)
}

export default Advertisement;