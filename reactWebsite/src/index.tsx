import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import RouteLink from './RouteLink';
import reportWebVitals from './reportWebVitals';
import {ConnectedRouter} from "connected-react-router";
import {store,history} from "./store";
import {Provider} from "react-redux";
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
	<React.StrictMode>
		<Provider store={store}>
			<ConnectedRouter history={history}>
			<RouteLink/>
			</ConnectedRouter>
		</Provider>
	</React.StrictMode>,
	document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
