import {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button, Input} from "reactstrap";
import "./UpdateCustomerData.scss";
import {updateCustomerData} from "./redux/editUsers/actions";
import {push} from "connected-react-router";
import {IRootState} from "./store";

function UpdateCustomerData() {
    const sidebar = useSelector((state: IRootState) => state.sidebar.sidebarToggle);
    const [isOutput, setIsOutput] = useState(false);
    const dispatch = useDispatch();
    const [userId, setUserId] = useState("");
    const [password, setPassword] = useState("");
    const [referral_agent_id, setReferral_agent_id] = useState("");
    const [date_of_birth, setDate_of_birth] = useState("");
    const [display_name, setDisplay_name] = useState("");
    const [email, setEmail] = useState("");
    const [is_deleted, setIs_deleted] = useState("");
    const [mobile, setMobile] = useState("");
    const [referral_mobile, setReferral_mobile] = useState("");
    const [username, setUsername] = useState("");
    const [gender_id, setGender_id] = useState("");
    const [role_id, setRole_id] = useState("");
    // const customerData = useSelector((state: IRootState) => state.editUsers.customerData);
    // console.log(customerData+" customerData before");

    return(
        <div className={sidebar ? "home-active" : "home"}>
			{/* input version content */}
            {isOutput === false && (
                <div>
                    <div className="treatment-title-container">
                        <div className="treatment-title">更新客戶資料</div>
                    </div>
                    <div className="flex-advertisement-detail-content">
                        <div className="advertisement-input-content-detail">id:</div>
                        <Input className="advertisement-input-detail-title" value={userId}
						onChange={(event) => {
							setUserId(event.currentTarget.value);
						}}
                        />
                    </div>
                    <div className="flex-advertisement-detail-content">
                        <div className="advertisement-input-content-detail">密碼:</div>
                        <Input className="advertisement-input-detail-title" value={password}
						onChange={(event) => {
							setPassword(event.currentTarget.value);
						}}
                        />
                    </div>
                    <div className="flex-advertisement-detail-content">
                        <div className="advertisement-input-content-detail">推薦中介人id:</div>
                        <Input className="advertisement-input-detail-title" value={referral_agent_id}
						onChange={(event) => {
							setReferral_agent_id(event.currentTarget.value);
						}}
                        />
                    </div>
                    <div className="flex-advertisement-detail-content">
                        <div className="advertisement-input-content-detail">出生日期:</div>
                        <Input className="advertisement-input-detail-title" value={date_of_birth}
						onChange={(event) => {
							setDate_of_birth(event.currentTarget.value);
						}}
                        />
                    </div>
                    <div className="flex-advertisement-detail-content">
                        <div className="advertisement-input-content-detail">顯示名稱:</div>
                        <Input className="advertisement-input-detail-title" value={display_name}
						onChange={(event) => {
							setDisplay_name(event.currentTarget.value);
						}}
                        />
                    </div>
                    <div className="flex-advertisement-detail-content">
                        <div className="advertisement-input-content-detail">電郵:</div>
                        <Input className="advertisement-input-detail-title" value={email}
						onChange={(event) => {
							setEmail(event.currentTarget.value);
						}}
                        />
                    </div>
                    <div className="flex-advertisement-detail-content">
                        <div className="advertisement-input-content-detail">客戶身份已刪除:</div>
                        <select className="treatment-detail-title" value={is_deleted}
                        onChange={(event) => {
                            setIs_deleted(event.currentTarget.value)
                        }}>
                            <option value=""></option>
                            <option value="true">是</option>
                            <option value="false">否</option>
                        </select>
                    </div>
                    <div className="flex-advertisement-detail-content">
                        <div className="advertisement-input-content-detail">手機號碼:</div>
                        <Input className="advertisement-input-detail-title" value={mobile}
						onChange={(event) => {
							setMobile(event.currentTarget.value);
						}}
                        />
                    </div>
                    <div className="flex-advertisement-detail-content">
                        <div className="advertisement-input-content-detail">推薦人手機號碼:</div>
                        <Input className="advertisement-input-detail-title" value={referral_mobile}
						onChange={(event) => {
							setReferral_mobile(event.currentTarget.value);
						}}
                        />
                    </div>
                    <div className="flex-advertisement-detail-content">
                        <div className="advertisement-input-content-detail">用戶名稱:</div>
                        <Input className="advertisement-input-detail-title" value={username}
						onChange={(event) => {
							setUsername(event.currentTarget.value);
						}}
                        />
                    </div>
                    <div className="flex-treatment-detail-content">
                        <div className="advertisement-input-content-detail">客戶性別:</div>
                        <select className="treatment-detail-title" value={gender_id}
                        onChange={(event) => {
                            setGender_id(event.currentTarget.value)
                        }}>
                            <option value=""></option>
                            <option value="1">男性</option>
                            <option value="2">女性</option>
                        </select>
                    </div>
                    <div className="flex-treatment-detail-content">
                        <div className="advertisement-input-content-detail">客戶類別:</div>
                        <select className="treatment-detail-title" value={role_id}
                        onChange={(event) => {
                            setRole_id(event.currentTarget.value)
                        }}>
                            <option value=""></option>
                            <option value="1">客戶</option>
                            <option value="2">客戶及中介</option>
                        </select>
                    </div>
                </div>
            )}
            {/* input version buttons */}
            {isOutput === false && (
                <div>
                    <Button color="danger" className="change-button" onClick={() => {
						dispatch(updateCustomerData(userId,password,referral_agent_id,date_of_birth,display_name,
						email,is_deleted,mobile,referral_mobile,username,gender_id,role_id));
						// console.log(customerData+" customerData after");
						setIsOutput(true);
					}}>
                        確認更新
                    </Button>
                    <Button color="danger" className="change-button" onClick={() => {
						dispatch(push("/editUser"));
						setIsOutput(false);
					}}>
                        返回編輯用戶頁
                    </Button>
                </div>
            )}
            {/* output version content */}
            {isOutput === true && (
                <div className="content-treatment-container">
                    <div className="treatment-title-container">
                        <div className="treatment-title">更新的客戶資料</div>
                    </div>
                    <div className="content-new-description">密碼:</div>
                    <div className="treatment-detail">{password}</div>
                    <div className="content-new-description">推薦中介人id:</div>
                    <div className="treatment-detail">{referral_agent_id}</div>
                    <div className="content-new-description">出生日期:</div>
                    <div className="treatment-detail">{date_of_birth}</div>
                    <div className="content-new-description">顯示名稱:</div>
                    <div className="treatment-detail">{display_name}</div>
                    <div className="content-new-description">電郵:</div>
                    <div className="treatment-detail">{email}</div>
                    <div className="content-new-description">客戶身份已鏟除:</div>
                    <div className="treatment-detail">{is_deleted}</div>
                    <div className="content-new-description">手機號碼:</div>
                    <div className="treatment-detail">{mobile}</div>
                    <div className="content-new-description">推薦人手機號碼:</div>
                    <div className="treatment-detail">{referral_mobile}</div>
                    <div className="content-new-description">用戶名稱:</div>
                    <div className="treatment-detail">{username}</div>
                    <div className="content-new-description">客戶性別:</div>
                    <div className="treatment-detail">{gender_id}</div>
                    <div className="content-new-description">客戶類別:</div>
                    <div className="treatment-detail">{role_id}</div>
                </div>
            )}
            {/* output version button */}
            {isOutput === true && (
                <div>
                    <Button color="info" className="change-button" onClick={() => {
                        dispatch(push("/editUser"));
                        setIsOutput(false);
                    }}>
                        返回編輯用戶頁
                    </Button>
                </div>
            )}
        </div>
    );
}

export default UpdateCustomerData;
