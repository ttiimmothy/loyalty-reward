import { CallHistoryMethodAction,push } from "connected-react-router";
import { Dispatch } from "redux";
import { AdvertisementDetail, AdvertisementSimple, Treatment } from "../../models";

export function getAllAdvertisements(pageDisplay:string,pageNumber:number) {
    return async (dispatch: Dispatch<IAdvertisementActions>) => {
        const token = localStorage.getItem("token");
        const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/advertisement?display=${pageDisplay}
        &page=${pageNumber}`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        const result = await res.json();
        if (!result.message) {
            dispatch(getAllAdvertisementsSuccess(result))
        } else {
            dispatch(getAllAdvertisementsFailure())
            dispatch(failed("Get_all_advertisements_failed", result.message))
        }
    }
}

export function getAdvertisementWithId(advertisementId: number | undefined) {
    return async (dispatch: Dispatch<IAdvertisementActions>) => {
        const token = localStorage.getItem("token");
        if (advertisementId) {
            const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/advertisement/${advertisementId}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            const result = await res.json();

            if (!result.message) {
                dispatch(getAdvertisementWithIdSuccess(result))
            } else {
                dispatch(failed("Get_advertisements_with_id_failed", result.message))
            }
        } else {
            const advertisementDetail = {
                image:"",
                advertisements_title:"",
                is_hidden:"",
                id:"",
                detail:"",
                treatments_title:"",
				treatment_id:""
            }
            dispatch(getAdvertisementWithIdSuccess(advertisementDetail));
        }
    }
}

export function updateAdvertisement(advertisementId:number,photo:File|null,is_hidden:string,title:string,treatment_id:string|undefined) {
    return async (dispatch: Dispatch<IAdvertisementActions|CallHistoryMethodAction>) => {
        const token = localStorage.getItem("token");
        let advUpdateForm = new FormData();
        // update photo if ys
        if (photo) {
            advUpdateForm.append("file", photo);
        }

        // append the form with mandatory data
        advUpdateForm.append("is_hidden", is_hidden);
        advUpdateForm.append("title", title);

        if(treatment_id){
            advUpdateForm.append("treatment_id", treatment_id);
        }

        const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/advertisement/update/${advertisementId}`, {
            method: "post",
            headers: {
                Authorization:`Bearer ${token}`
            },
            body:advUpdateForm
        })
        const result = await res.json();
        console.log(result)
        if (result.success) {
            dispatch(updateAdvertisementSuccess());
            setTimeout(() => {
                dispatch(push("/advertisement"))
            }, 2000)
        } else {
            dispatch(updateAdvertisementFailure());
            dispatch(failed("Update_advertisement_failed", result.message));
        }
    }
}

export function createAdvertisement(photo:File|null,is_hidden:string,title:string,treatment_id:string|undefined) {
    return async (dispatch: Dispatch<IAdvertisementActions|CallHistoryMethodAction>) => {
        const token = localStorage.getItem("token");

        // assign a name to this FormData
        let advCreateForm = new FormData();

        // update photo if ys
        if (photo) {
            advCreateForm.append("file", photo);
        }

        // create fail if anyone is missing
        if (!is_hidden || !title || !treatment_id) {
            dispatch(createAdvertisementFailure());
            return;
        }

        // append the form with mandatory data
        advCreateForm.append("is_hidden", is_hidden);
        advCreateForm.append("title", title);
        advCreateForm.append("treatment_id", treatment_id);

        const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/advertisement/create`, {
            method: "post",
            headers: {
                Authorization: `Bearer ${token}`
            },
            body: advCreateForm
        })
        const result = await res.json();
        console.log(result);
        if (result.success) {
            dispatch(createAdvertisementSuccess());
            setTimeout(() => {
                dispatch(push("/advertisement"));
            }, 2000)
        } else {
            dispatch(createAdvertisementFailure());
            dispatch(failed("Create_advertisement_failed", result.message));
        }
    }
}

export function resetUpdateNull(){
    return{
        type:"@@advertisement/Reset_update_null" as const,
    }
}

export function resetCreateNull(){
    return{
        type:"@@advertisement/Reset_create_null" as const,
    }
}

export function updateAdvertisementTitle(advTitle:string){
    return{
		type:"@@advertisement/Update_advertisement_title" as const,
		advTitle
    }
}

export function updateAdvertisementDetail(advDetail:string){
    return{
		type:"@@advertisement/Update_advertisement_detail" as const,
		advDetail
    }
}

export function updateAdvTreatmentsId(advTreatmentId:string){
    return{
		type:"@@advertisement/Update_advertisement_treatment_id" as const,
		advTreatmentId
    }
}

export function updateHidden(value:string){
    return{
		type:"@@advertisement/Update_hidden" as const,
		value
    }
}

export function getAllAdvertisementsSuccess(allAdvertisements: AdvertisementSimple[]) {
    return {
        type: "@@advertisement/Get_all_advertisements" as const,
        allAdvertisements
    }
}

export function getAllAdvertisementsFailure(){
    return{
		type:"@@treatment/Get_all_advertisements_fail" as const,
	}
}

export function getAdvertisementWithIdSuccess(advertisementDetail: AdvertisementDetail) {
    return {
        type: "@@advertisement/Get_advertisements_with_id" as const,
        advertisementDetail
    }
}

export function updateAdvertisementSuccess() {
    return {
        type: "@@advertisement/Update_advertisement" as const,
    }
}

export function updateAdvertisementFailure() {
    return {
        type: "@@advertisement/Update_advertisement_fail" as const,
    }
}

export function createAdvertisementSuccess() {
    return {
        type: "@@advertisement/Create_advertisement" as const,
    }
}

export function createAdvertisementFailure() {
    return {
        type: "@@advertisement/Create_advertisement_fail" as const,
    }
}

export function updateAdvertisementTreatId(treatment:Treatment[]){
	return {
        type: "@@advertisement/Update_advertisement_treatment_all_id" as const,
		treatment
    }
}

type Failed = "Get_all_advertisements_failed"|"Get_advertisements_with_id_failed"|"Update_advertisement_failed"|
"Create_advertisement_failed"
function failed(type: Failed, message: string) {
    return {
        type,
        message
    }
}

export type IAdvertisementActions = ReturnType<typeof getAllAdvertisementsSuccess|typeof getAdvertisementWithIdSuccess|
typeof updateAdvertisementSuccess|typeof updateAdvertisementFailure|typeof createAdvertisementSuccess|
typeof createAdvertisementFailure|typeof getAllAdvertisementsFailure|
typeof resetUpdateNull|typeof resetCreateNull|typeof updateAdvertisementTitle|typeof updateAdvertisementDetail|
typeof updateAdvTreatmentsId|typeof updateHidden|typeof updateAdvertisementTreatId|
typeof failed>

