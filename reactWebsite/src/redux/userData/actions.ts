import {CallHistoryMethodAction,push} from "connected-react-router";
import {Dispatch} from "redux";
// import { UserData } from "./reducer";

export function fetchUserData() {
    return async (dispatch:Dispatch<IUserDataActions>) => {
		const token = localStorage.getItem("token")
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/user?display=3&page=1`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		console.log(result)
		if(result.success){
			dispatch(fetchUserDataSuccess(result.data))
		}
    }
}

export function userDataUpdate(display_name:string,self_intro:string,photo:File|null|undefined) {
    return async (dispatch:Dispatch<IUserDataActions|CallHistoryMethodAction>) => {
		const token = localStorage.getItem('token')
		let userDataUpdate = new FormData();
		if(photo){
			userDataUpdate.append("photo",photo);
		}
		userDataUpdate.append("display_name",display_name)
		userDataUpdate.append("self_intro",self_intro)

		await fetch(`${process.env.REACT_APP_BACKEND_HOST}/users/userDataUpdate`,{
			method:"post",
			headers:{
				Authorization:`Bearer ${token}`
			},
			body:userDataUpdate,
		})
		dispatch(push("/"))
    }
}

export function fetchUserDataSuccess(userData:any) {
    return {
        type:"@@userData/Load_user_data" as const,
        userData
    }
}

export type IUserDataActions = ReturnType<typeof fetchUserDataSuccess>