import { Dispatch } from "redux";

export function requestResetLink(email:string){
    return async(dispatch:Dispatch<IResetPasswordActions>) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/users/resetPassword/`,{
            method:"post",
            headers:{
                "Content-Type": "application/json"
            },
            body:JSON.stringify({email})
        });
        const result = await res.json();
        // console.log(result)
        if(result.success){
            dispatch(requestResetLinkSuccess(result.success));
        }else{
            dispatch(requestResetLinkFailure(result.message));
        }
    }
}



export function resetPassword(uuId:string,password:string){
    return async(dispatch:Dispatch<IResetPasswordActions>) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/users/resetPassword/${uuId}`,{
            method:"post",
            headers:{
                "Content-Type": "application/json"
            },
            body:JSON.stringify({password})
        });
        const result = await res.json();
        // console.log(result)
        if(result.success){
            dispatch(resetPasswordSuccess());
        }else{
            dispatch(resetPasswordFailure(result.message));
        }
    }
}


export function requestResetLinkSuccess(success:string){
    return{
        type:"@@resetPassword/Request_reset_link_success" as const,
        success
    }
}

export function requestResetLinkFailure(message:string){
    return{
        type:"@@resetPassword/Request_reset_link_failure" as const,
        message
    }
}

export function resetIsRequestedNull(){
    return async (dispatch:Dispatch<IResetPasswordActions>)=>{
        dispatch(resetIsRequestNullSuccess());
    }
}

export function resetIsRequestNullSuccess(){
    return{
        type:"@@resetPassword/Reset_isRequest_null" as const,
    }
}

export function resetPasswordSuccess(){
    return{
        type:"@@resetPassword/Reset_password_success" as const
    }
}
export function resetPasswordFailure(message:string){
    return{
        type:"@@resetPassword/Reset_password_failure" as const,
        message
    }
}

export function resetNull(){
    return async (dispatch:Dispatch<IResetPasswordActions>)=>{
        dispatch(resetNullSuccess());
    }
}

export function resetNullSuccess(){
    return{
        type:"@@resetPassword/Reset_null" as const,
    }
}

type Failed = ""
function failed(type: Failed, message: string) {
    return {
        type,
        message
    }
}

export type IResetPasswordActions = ReturnType<typeof resetPasswordSuccess|typeof resetPasswordFailure|
typeof resetNullSuccess|typeof requestResetLinkSuccess|typeof requestResetLinkFailure|typeof resetIsRequestNullSuccess|
typeof failed>