import {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button, Input} from "reactstrap";
import "./Login.scss";
import {login,resetLoggedInNull} from "./redux/auth/actions";
import {IRootState} from "./store";
import trophy from "./photos/trophy.png"

function Login(){
	const [username,setUsername] = useState("");
	const [password,setPassword] = useState("");
	// console.log(username);
	// console.log(password);
	// const isAuthenticated = useSelector((state:IRootState)=>state.auth.isAuthenticated);
	const Login = useSelector((state:IRootState)=>state.auth.isLoggedIn)
	// console.log(isAuthenticated)
	const dispatch = useDispatch();
	return(
		<div className="login-container">
			<div className="login-middle-container">
				<div>
					<div className="title">歡迎來到Kenson主控台</div>
					<img src={trophy} alt="" className="image"/>
					<div className="input-header">
						<div>用戶名</div>
						<Input className="input" value={username} onChange={(event) => {
							dispatch(resetLoggedInNull())
							setUsername(event.currentTarget.value)
						}}/>
					</div>
					<div  className="input-header">
						<div>密碼</div>
						<Input className="input" value={password} onChange={(event) => {
							dispatch(resetLoggedInNull())
							setPassword(event.currentTarget.value)
						}}/>
					</div>
					<div className="login-button">
						<Button color="success" onClick={() => {
							dispatch(login(username,password))
						}}>登入</Button>
					</div>
				</div>
				{Login === false && <div className="warning-container">
					<div className="warning-message">用戶名或密碼錯誤</div>
				</div>}
			</div>
		</div>
	)
}

export default Login;