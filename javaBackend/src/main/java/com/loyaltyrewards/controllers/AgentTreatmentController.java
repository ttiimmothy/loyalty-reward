package com.loyaltyrewards.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.models.user.Gender;
import com.loyaltyrewards.models.user.User;
import com.loyaltyrewards.repositories.user.GenderRepository;
import com.loyaltyrewards.repositories.user.UserRepository;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Component
@RequestMapping("/agents/treatment")
public class AgentTreatmentController {
    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;

    @Autowired
    UserRepository userRepository;

    @Autowired
    GenderRepository genderRepository;

    @GetMapping("/all")
    public ResponseEntity<String> getAvailableTreatment(HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long agent_id = jwt.getAgent_id();
        if (agent_id == 0) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not an agent");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{
            String sql = String.format("SELECT treatments.id,treatments.title,treatments.image " +
                            "from treatments " +
                            "where (is_hidden=false)");
            System.out.println(sql);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);
            System.out.println(result);
            return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping
    public ResponseEntity<String> getAvailableTreatmentFilter(@RequestParam Integer service,@RequestParam Integer branch,HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long agent_id = jwt.getAgent_id();
        if (agent_id == 0) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not an agent");
            return ResponseEntity.status(400).body(json.toString());
        }

        String serviceCondition="";
        String serviceSelect="";
        String serviceJoin="";
        String branchCondition="";
        String branchSelect="";
        String branchJoin="";
        if (service > 0) {
            serviceCondition = " and services.id=" + service;
            serviceSelect = "";
            serviceJoin = "left join treatment_service_relation ON treatment_service_relation.treatment_id = treatments.id left join services ON treatment_service_relation.service_id = services.id ";
        }
        if (branch >0 ) {
            branchCondition = " and branches.id=" + branch;
            branchSelect = "";
            branchJoin = "left join treatment_branch_relation ON treatment_branch_relation.treatment_id = treatments.id left join branches ON treatment_branch_relation.branch_id = branches.id ";
        }


        String condition = serviceCondition+branchCondition;
        System.out.println(condition);
        try{
            String sql = String.format("SELECT treatments.id,treatments.title,treatments.image " +
                            "from treatments " +
                            "%3$s " +
                            "%5$s " +
                            "where ( true %1$s and is_hidden=false)",
                    condition,serviceSelect,serviceJoin,branchSelect,branchJoin);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);
            return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/branch")
    public ResponseEntity<String> getAllBranch(HttpServletRequest request) {
        try{
            String sql = String.format("SELECT name,id " +
                    "from branches");
            System.out.println(sql);
        List<ObjectNode> result = this.sqlService.selectQuery(sql);
        System.out.println(result);
        return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }
    @GetMapping("/service")
    public ResponseEntity<String> getAllServiceType(HttpServletRequest request) {
        try{
            String sql = String.format("SELECT service,id " +
                    "from services");
            System.out.println(sql);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);
            System.out.println(result);
            return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<String> getServiceWithID(@PathVariable Long id,HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long agent_id = jwt.getAgent_id();
        if (agent_id == 0) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not an agent");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{
            String sql = String.format("SELECT id,detail,image,title " +
                    "from treatments " +
                    "where id = %1$s ",
                    id);
            System.out.println(sql);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);
            System.out.println(result);
            return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }


}
