package com.loyaltyrewards.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.repositories.user.GenderRepository;
import com.loyaltyrewards.repositories.user.UserRepository;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Component
@RequestMapping("/points")
public class PointsController {
    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;

    @Autowired
    UserRepository userRepository;

    @Autowired
    GenderRepository genderRepository;

    @GetMapping("/history/redeemed")
    public ResponseEntity<String> getRedeemed(HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long customer_id = jwt.getCustomer_id();

        try{
            String sql = String.format("SELECT " +
                    "redeem_order.order_ref," +
                    "redeem_order.create_time as date, " +
                    "pickup_location.name as location, " +
                    "total_redeem_points as points " +
                    "from redeem_order " +
                    "left join pickup_location ON pickup_location.id = pickup_location_id " +
                    "where (is_deleted=false and customer_id = %1$s ) " ,
                    customer_id);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);


            return ResponseEntity.status(200).body(result.toString());

        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }
    @GetMapping("/history/earned")
    public ResponseEntity<String> getEarned(HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long customer_id = jwt.getCustomer_id();

        try{

            String sql = String.format("SELECT " +
                            "earned_point.id," +
                            "earned_point.create_time as date, " +
                            "treatments.title as summary, " +
                            "branches.name as location, " +
                            "earned_point.points as points " +
                            "from earned_point " +
                            "left join appointments ON appointments.id = appointment_id " +
                            "left join branches ON branches.id = branch_id " +
                            "left join treatments ON treatments.id = treatment_id " +
                            "where (earned_point.customer_id = %1$s and earned_point.is_deleted = false)",
                    customer_id);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);


            return ResponseEntity.status(200).body(result.toString());

        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }
    @GetMapping("/history/expired")
    public ResponseEntity<String> getExpired(HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long customer_id = jwt.getCustomer_id();

        try{
            String sql = String.format("SELECT " +
                            "expired_point.id," +
                            "expired_point.create_time as date, " +
                            "expired_point.points as points " +
                            "from expired_point " +
                            "where (customer_id = %1$s )",
                    customer_id);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);




            return ResponseEntity.status(200).body(result.toString());

        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }


    @GetMapping("/history/redeemed/{id}")
    public ResponseEntity<String> getRedeemedDetail(@PathVariable Long id, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long customer_id = jwt.getCustomer_id();

        try{
            String sql1 = String.format("SELECT " +
                            "order_ref," +
                            "redeem_order.create_time as date, " +
                            "pickup_location.name as location, " +
                            "total_redeem_points as points, " +
                            "is_pickup, " +
                            "CONCAT('/QR/orderRef/', customer_id, '/' , order_ref ) as QR_code " +
                            "from redeem_order " +
                            "left join pickup_location ON pickup_location.id = pickup_location_id " +
                            "where (is_deleted=false and customer_id = %1$s and order_ref = %2$s) " ,
                    customer_id,id);
            JSONObject result1 = this.sqlService.selectQuery2(sql1).get(0);
            String sql2 = String.format("SELECT " +
                            "amount," +
                            "order_ref, " +
                            "image, " +
                            "name, " +
                            "remain_amount, " +
                            "redeem_points, " +
                            "reward_type_id " +
                            "from redeem_items " +
                            "left join rewards ON rewards.id = reward_id " +
                            "where (order_ref = %1$s) " ,
                    id);
            List<JSONObject> result2 = this.sqlService.selectQuery2(sql2);

            JSONObject json=new JSONObject();
            json.put("branch",result1);
            json.put("redeem_items",result2);
            return ResponseEntity.status(200).body(json.toString());

        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

}
