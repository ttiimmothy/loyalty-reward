package com.loyaltyrewards.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.loyaltyrewards.lib.ConvertFormat;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Component
@RequestMapping("/index")
public class HomePageController {

    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;

    @Autowired
    ConvertFormat convertFormat;

    @GetMapping("/advertisements")
    public ResponseEntity<String> getAdvertisement() {
        try{
            String sql = "SELECT id,title,image,treatment_id from advertisements " +
                    "where is_hidden = false";
            List<ObjectNode> result = this.sqlService.selectQuery(sql);
            return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/treatments")
    public ResponseEntity<String> getPopularTreatments() {
        try{
            String sql = "SELECT treatments.id,image,title,count(appointments.id) as no_of_appointment " +
                    "from treatments " +
                    "LEFT JOIN appointments on appointments.treatment_id = treatments.id " +
                    "where is_hidden = 'false' " +
                    "group by treatments.id,image,title " +
                    "ORDER by no_of_appointment desc " +
                    "LIMIT 5 OFFSET 0";
            List<ObjectNode> result = this.sqlService.selectQuery(sql);
            return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/prediction")
    public ResponseEntity<String> getPredictedTreatments(HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long customer_id = jwt.getCustomer_id();
        try{
            String my_group_sql = "Select grouping from python_group where customer_id =" + customer_id;
            List<ObjectNode> my_group_sql_result = this.sqlService.selectQuery(my_group_sql);
            String jsonStr = my_group_sql_result.get(0).toString();
            JSONObject resultJson = new JSONObject(jsonStr);
            String grouping = resultJson.getString("grouping");

            String sql = String.format("SELECT treatments.id,image,title,count(appointments.id) as no_of_appointment " +
                    "from treatments " +
                    "LEFT JOIN appointments on appointments.treatment_id = treatments.id " +
                    "LEFT JOIN customers on appointments.customer_id = customers.id " +
                    "LEFT JOIN python_group on python_group.customer_id = customers.id " +
                    "where is_hidden = 'false' and python_group.grouping = %1$s " +
                    "group by treatments.id,image,title " +
                    "ORDER by no_of_appointment desc " +
                    "LIMIT 5 OFFSET 0",grouping);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);
            return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/services")
    public ResponseEntity<String> getTop6Service() {
        try {
            String sql = "SELECT id,service,image from services " +
                    "LIMIT 6 OFFSET 0";
            List<ObjectNode> result = this.sqlService.selectQuery(sql);
            return ResponseEntity.status(200).body(result.toString());

        } catch (Exception e) {
            JSONObject json = new JSONObject();
            json.put("success", false);
            json.put("error", e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/points")
    public ResponseEntity<String> getRedeemPoints(HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long customer_id = jwt.getCustomer_id();
        try {
            String sql1 = "SELECT sum(points) as points " +
                    "from customers " +
                    "left join earned_point on earned_point.customer_id = customers.id " +
                    "where earned_point.is_deleted = false and customer_id = " + customer_id;
            List<ObjectNode> result1 = this.sqlService.selectQuery(sql1);

            String sql2 = "SELECT sum(points) as points  " +
                    "from customers " +
                    "left join expired_point on expired_point.customer_id = customers.id " +
                    "where customer_id = " + customer_id;
            List<ObjectNode> result2 = this.sqlService.selectQuery(sql2);

            String sql3 = "SELECT sum(total_redeem_points) as points " +
                    "from customers " +
                    "left join redeem_order on redeem_order.customer_id = customers.id " +
                    "where customer_id = " + customer_id;
            List<ObjectNode> result3 = this.sqlService.selectQuery(sql3);

            Integer earned_point = convertFormat.ParseInt(result1.get(0).get("points").toString().replaceAll("\"",""));
            Integer expired_point = convertFormat.ParseInt(result2.get(0).get("points").toString().replaceAll("\"",""));
            Integer redeem_order = convertFormat.ParseInt(result3.get(0).get("points").toString().replaceAll("\"",""));
            Integer current_point = earned_point-expired_point-redeem_order;

            JSONObject json = new JSONObject();
            json.put("success", true);
            json.put("current_point", current_point);
            return ResponseEntity.status(200).body(json.toString());

        } catch (Exception e) {
            JSONObject json = new JSONObject();
            json.put("success", false);
            json.put("error", e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

}
