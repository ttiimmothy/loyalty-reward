package com.loyaltyrewards.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.loyaltyrewards.lib.ConvertFormat;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.models.form.PickupLocationForm;
import com.loyaltyrewards.models.form.UserForm;
import com.loyaltyrewards.models.redeem.CartItem;
import com.loyaltyrewards.models.redeem.Reward;
import com.loyaltyrewards.repositories.redeem.CartItemRepository;
import com.loyaltyrewards.repositories.user.GenderRepository;
import com.loyaltyrewards.repositories.user.UserRepository;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@RestController
@Component
@RequestMapping("/cart")
public class CartController {
    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;

    @Autowired
    CartItemRepository cartItemRepository;

    @Autowired
    GenderRepository genderRepository;

    @Autowired
    ConvertFormat convertFormat;

    @PostMapping("/add/{id}")
    public ResponseEntity<String> addItemToCart(@PathVariable Long id,HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long customer_id = jwt.getCustomer_id();

        try{
            String tableName = "cart_item";
            String columnName = "customer_id,rewards_id";
            String columnData = String.format("%1$s,%2$s",
                    customer_id,id);
            Long cart_id = sqlService.insertQuery(tableName, columnName, columnData);
            JSONObject json=new JSONObject();
            json.put("success",true);
            json.put("cart_id",cart_id);
            return ResponseEntity.status(200).body(json.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @PutMapping("/minus/{reward_id}")
    public ResponseEntity<String> minusItemToCart(@PathVariable Long reward_id,HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long customer_id = jwt.getCustomer_id();

        try{
            String tableName = "cart_item";
            String columnName = "is_removed,is_checked_out";
            String columnData = "true,false";
            String updateCondition = String.format("customer_id=%1$s and rewards_id=%2$s ",customer_id,reward_id);
            String firstRowCondition = String.format("customer_id=%1$s and rewards_id=%2$s and is_checked_out=false and is_removed=false ", customer_id,reward_id);
            Long cart_id = sqlService.updateOneQuery(tableName, columnName, columnData,updateCondition,firstRowCondition);
            JSONObject json=new JSONObject();
            json.put("success",true);
            json.put("cart_id",cart_id);
            return ResponseEntity.status(200).body(json.toString());
        }catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","cart empty");
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @PutMapping("/delete/{reward_id}")
    public ResponseEntity<String> deleteItemToCart(@PathVariable Long reward_id,HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long customer_id = jwt.getCustomer_id();

        System.out.println("Delete cart item");
        try{
            String tableName = "cart_item";
            String columnName = "is_removed,is_checked_out";
            String columnData = "true,false";
            String updateCondition = String.format("customer_id=%1$s and rewards_id=%2$s ",customer_id,reward_id);
            sqlService.updateQuery(tableName, columnName, columnData,updateCondition);
            JSONObject json=new JSONObject();
            json.put("success",true);
            return ResponseEntity.status(200).body(json.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/inStock")
    public ResponseEntity<String> getInStockItem(HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long customer_id = jwt.getCustomer_id();

        try{
            String sql = String.format("SELECT rewards.id,name,image,redeem_points,reward_type.type,remain_amount,count(cart_item.id) as amount_in_cart,max(cart_item.id) as latest_cart_id " +
                    "from rewards " +
                    "left join reward_type ON reward_type.id = reward_type_id " +
                    "left join cart_item ON rewards.id = cart_item.rewards_id " +
                    "left join customers ON customers.id = %1$s " +
                    "where (is_checked_out=false and is_removed=false and customers.id = customer_id) " +
                    "group by (rewards.id,name,image,redeem_points,reward_type.type,remain_amount) " +
                    "having remain_amount>=count(cart_item.id)", customer_id);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);
            return ResponseEntity.status(200).body(result.toString());

        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/outOfStock")
    public ResponseEntity<String> getOutOfStockItem(HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long customer_id = jwt.getCustomer_id();

        try{
            String sql = String.format("SELECT rewards.id,name,image,redeem_points,reward_type.type,remain_amount,count(cart_item.id) as amount_in_cart,max(cart_item.id)  as latest_cart_id " +
                    "from rewards " +
                    "left join reward_type ON reward_type.id = reward_type_id " +
                    "left join cart_item ON rewards.id = cart_item.rewards_id " +
                    "left join customers ON customers.id = %1$s " +
                    "where (is_checked_out=false and is_removed=false and customers.id = customer_id) " +
                    "group by (rewards.id,name,image,redeem_points,reward_type.type,remain_amount) " +
                    "having remain_amount<count(cart_item.id)", customer_id);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);
            return ResponseEntity.status(200).body(result.toString());

        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @PostMapping("/redeem")
    public ResponseEntity<String> redeemOrder(@RequestBody PickupLocationForm form, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long customer_id = jwt.getCustomer_id();
        try{
            String sql1s = "SELECT sum(points) as points " +
                    "from customers " +
                    "left join earned_point on earned_point.customer_id = customers.id " +
                    "where earned_point.is_deleted = false and customer_id = " + customer_id;
            List<ObjectNode> result1s = this.sqlService.selectQuery(sql1s);

            String sql2s = "SELECT sum(points) as points  " +
                    "from customers " +
                    "left join expired_point on expired_point.customer_id = customers.id " +
                    "where customer_id = " + customer_id;
            List<ObjectNode> result2s = this.sqlService.selectQuery(sql2s);

            String sql3s = "SELECT sum(total_redeem_points) as points " +
                    "from customers " +
                    "left join redeem_order on redeem_order.customer_id = customers.id " +
                    "where customer_id = " + customer_id;
            List<ObjectNode> result3s = this.sqlService.selectQuery(sql3s);

            Integer earned_point = convertFormat.ParseInt(result1s.get(0).get("points").toString().replaceAll("\"",""));
            Integer expired_point = convertFormat.ParseInt(result2s.get(0).get("points").toString().replaceAll("\"",""));
            Integer redeem_order = convertFormat.ParseInt(result3s.get(0).get("points").toString().replaceAll("\"",""));
            Integer current_point = earned_point-expired_point-redeem_order;
            if (convertFormat.ParseInt(form.getTotal_redeem_points().toString().replaceAll("\"","")) > current_point) {
                JSONObject json=new JSONObject();
                json.put("success",false);
                json.put("message","not enough points");
                return ResponseEntity.status(400).body(json.toString());
            }

        // get all in stock item in cart
        String sql = String.format("SELECT rewards.id,name,image,redeem_points,reward_type.type,remain_amount,count(cart_item.id) as amount_in_cart " +
                "from rewards " +
                "left join reward_type ON reward_type.id = reward_type_id " +
                "left join cart_item ON rewards.id = cart_item.rewards_id " +
                "left join customers ON customers.id = %1$s " +
                "where (is_checked_out=false and is_removed=false and customers.id = customer_id) " +
                "group by (rewards.id,name,image,redeem_points,reward_type.type,remain_amount) " +
                "having remain_amount>=count(cart_item.id)", customer_id);
        List<ObjectNode> result = this.sqlService.selectQuery(sql);

        // Check empty Cart
        if (result.size() == 0) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Empty Cart");
            return ResponseEntity.status(400).body(json.toString());
        }

        Random rnd = new Random();
        Integer number = rnd.nextInt(999);
        String randomString = String.format("%03d", number);
        String reference_number = (System.currentTimeMillis()+randomString);

        for (int i = 0; i < result.size(); i = i + 1) {
            // transfer all cart item to redeem item
            String jsonStr = result.get(i).toString();
            JSONObject jObject = new JSONObject(jsonStr);
            String amount = jObject.getString("amount_in_cart");
            String reward_id = jObject.getString("id");
            String redeemTableName = "redeem_items";
            String redeemColumnName = "amount,order_ref,reward_id";
            String redeemColumnData = String.format("%1$s,%2$s,%3$s",
                    amount,reference_number,reward_id);
            sqlService.insertQuery(redeemTableName, redeemColumnName, redeemColumnData);
            String cartTableName = "cart_item";
            String cartColumnName = "is_removed,is_checked_out";
            String cartColumnData = "false,true";
            String updateCondition = String.format("customer_id=%1$s and rewards_id=%2$s ",customer_id,reward_id);
            sqlService.updateQuery(cartTableName, cartColumnName, cartColumnData,updateCondition);
        }

        String orderTableName = "redeem_order";
        String orderColumnName = "order_ref," +
                "redeem_date," +
                "redeem_expires_date," +
                "total_redeem_points," +
                "customer_id," +
                "pickup_location_id";
        String orderColumnData = String.format("%1$s,%2$s,%3$s,%4$s,%5$s,%6$s",
                reference_number,
                "'"+LocalDate.now() + " 00:00:00'",
                "'"+LocalDate.now().plusDays(14) + " 00:00:00'",
                form.getTotal_redeem_points(),
                customer_id,
                form.getPickup_location_id());
        sqlService.insertQuery(orderTableName, orderColumnName, orderColumnData);


            String sql1 = String.format("SELECT " +
                            "order_ref," +
                            "redeem_order.create_time as date, " +
                            "pickup_location.name as location, " +
                            "total_redeem_points as points, " +
                            "is_pickup, " +
                            "CONCAT('/QR/orderRef/', customer_id, '/' , order_ref ) as QR_code " +
                            "from redeem_order " +
                            "left join pickup_location ON pickup_location.id = pickup_location_id " +
                            "where (is_deleted=false and customer_id = %1$s and order_ref = %2$s) " ,
                    customer_id,reference_number);
            JSONObject result1 = this.sqlService.selectQuery2(sql1).get(0);
            String sql2 = String.format("SELECT " +
                            "amount," +
                            "order_ref, " +
                            "image, " +
                            "name, " +
                            "remain_amount, " +
                            "redeem_points, " +
                            "reward_type_id " +
                            "from redeem_items " +
                            "left join rewards ON rewards.id = reward_id " +
                            "where (order_ref = %1$s) " ,
                    reference_number);
            List<JSONObject> result2 = this.sqlService.selectQuery2(sql2);

            JSONObject json=new JSONObject();
            json.put("branch",result1);
            json.put("redeem_items",result2);
            return ResponseEntity.status(200).body(json.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }
}
