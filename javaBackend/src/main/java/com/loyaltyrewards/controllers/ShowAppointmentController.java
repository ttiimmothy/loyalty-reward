package com.loyaltyrewards.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.loyaltyrewards.lib.MongoDB;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
@RestController
@Component
@RequestMapping("/schedule")
public class ShowAppointmentController {
    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;

    @Autowired
    MongoDB mongoDB;

    @GetMapping
    public ResponseEntity<String> getPendingAppointment(@RequestParam String status,HttpServletRequest request) {
            JWT jwt = decodeJWT.decode(request);
            Long customer_id = jwt.getCustomer_id();

        try{

            String sql;
            if (status.equals("pending")) {
                sql = String.format("SELECT appointments.id as id,timeslot,treatment_id,branches.name,treatments.title,treatments.image " +
                                "from appointments " +
                                "left join branches ON branches.id = branch_id " +
                                "left join treatments ON treatments.id = treatment_id " +
                                "left join scores ON appointments.id = scores.appointment_id " +
                                "where (is_completed = %1$s " +
                                "and is_cancelled = %2$s " +
                                "and is_scored = %3$s " +
                                "and customer_id = %4$s)",
                        false, false,false, customer_id);
            } else if (status.equals("completedScored")) {
                sql = String.format("SELECT appointments.id as id,timeslot,treatment_id,branches.name,treatments.title,score,treatments.image " +
                                "from appointments " +
                                "left join branches ON branches.id = branch_id  " +
                                "left join treatments ON treatments.id = treatment_id " +
                                "left join scores ON appointments.id = scores.appointment_id " +
                                "where (is_completed = %1$s " +
                                "and is_cancelled = %2$s " +
                                "and is_scored = %3$s " +
                                "and customer_id = %4$s)",
                        true, false, true, customer_id);
            } else if (status.equals("completedUnScored")) {
                sql = String.format("SELECT appointments.id as id,timeslot,treatment_id,branches.name,treatments.title,treatments.image " +
                                "from appointments " +
                                "left join branches ON branches.id = branch_id  " +
                                "left join treatments ON treatments.id = treatment_id " +
                                "left join scores ON appointments.id = scores.appointment_id " +
                                "where (is_completed = %1$s " +
                                "and is_cancelled = %2$s " +
                                "and is_scored = %3$s " +
                                "and customer_id = %4$s)",
                        true, false,false, customer_id);
            } else if (status.equals("cancelled")) {
                sql = String.format("SELECT appointments.id as id,timeslot,treatment_id,branches.name,treatments.title,treatments.image " +
                                "from appointments " +
                                "left join branches ON branches.id = branch_id  " +
                                "left join treatments ON treatments.id = treatment_id " +
                                "left join scores ON appointments.id = scores.appointment_id " +
                                "where (is_completed = %1$s " +
                                "and is_cancelled = %2$s " +
                                "and is_scored = %3$s " +
                                "and customer_id = %4$s)",
                        false, true,false, customer_id);
            } else {
                JSONObject json=new JSONObject();
                json.put("success",false);
                json.put("message","Status Error");
                return ResponseEntity.status(400).body(json.toString());
            }

            List<ObjectNode> result = this.sqlService.selectQuery(sql);

            try {
                mongoDB.insertToMongoDB("getLog",customer_id,"appointment-" + status, "all");
            } catch (Exception e) {
                System.err.println("mongoDB not found");
                System.err.println(e);
            }

            return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/cancel")
    public ResponseEntity<String> cancelAppointment(@RequestParam Long appointment_id,HttpServletRequest request) {
        try{
            JWT jwt = decodeJWT.decode(request);
            Long customer_id = jwt.getCustomer_id();
            String sql = String.format("UPDATE appointments SET is_cancelled = true " +
                            "where (id = %1$s and customer_id = %2$s and is_cancelled = false and is_completed = false and is_scored = false) returning id",
                    appointment_id, customer_id);
            System.out.println(sql);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);

            JSONObject json=new JSONObject();
            if (result.size() > 0 ) {
                json.put("success",true);
                return ResponseEntity.status(200).body(json.toString());
            } else {
                json.put("success",false);
                return ResponseEntity.status(400).body(json.toString());
            }
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/pending/{id}")
    public ResponseEntity<String> appointmentDetail(@PathVariable Long id,HttpServletRequest request) {
        try{
            JWT jwt = decodeJWT.decode(request);
            Long customer_id = jwt.getCustomer_id();

            String sql = String.format("SELECT appointments.id as id,timeslot,treatment_id,branches.name,branches.address,treatments.title,treatments.image, " +
                            "CONCAT('/QR/appointment/', customer_id, '/' , appointments.id ) as QR_code " +
                            "from appointments " +
                            "left join branches ON branches.id = branch_id " +
                            "left join treatments ON treatments.id = treatment_id " +
                            "where (is_completed = %1$s " +
                            "and is_cancelled = %2$s " +
                            "and is_scored = %3$s " +
                            "and customer_id = %4$s " +
                            "and appointments.id = %5$s)",
                    false, false, false, customer_id,id);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);
            Object toJson = result.get(0);
            return ResponseEntity.status(200).body(toJson.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","ID invalid");
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/score")
    public ResponseEntity<String> scoreAppointment(@RequestParam Long appointment_id, @RequestParam Integer score,HttpServletRequest request) {
        try{
            JWT jwt = decodeJWT.decode(request);
            Long customer_id = jwt.getCustomer_id();

            String sql = String.format("UPDATE appointments SET is_scored = true " +
                            "where (id = %1$s and customer_id = %2$s and is_scored = false  and is_cancelled = false and is_completed = true ) returning treatment_id",
                    appointment_id, customer_id);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);

            Long treatment_id = Long.parseLong(result.get(0).get("treatment_id").toString().replace("\"", ""));

            String tableName = "scores";
            String columnName = "score,appointment_id,treatments_id";
            String columnData = String.format("%1$s,%2$s,%3$s",
                    score,appointment_id,treatment_id);
            sqlService.insertQuery(tableName, columnName, columnData);

            JSONObject json=new JSONObject();
            if (result.size() > 0 ) {
                json.put("success",true);
                return ResponseEntity.status(200).body(json.toString());
            } else {
                json.put("success",false);
                return ResponseEntity.status(400).body(json.toString());
            }
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","score fail");
            return ResponseEntity.status(400).body(json.toString());
        }
    }
}
