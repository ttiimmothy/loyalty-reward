package com.loyaltyrewards.repositories.user;

import com.loyaltyrewards.models.user.Gender;
import com.loyaltyrewards.models.user.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface GenderRepository extends CrudRepository<Gender, Long> {
    Optional<Gender> findByGender(String gender);
    Optional<Gender> findByUsers(User user);
}
