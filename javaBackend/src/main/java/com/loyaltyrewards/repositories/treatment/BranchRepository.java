package com.loyaltyrewards.repositories.treatment;


import com.loyaltyrewards.models.treatment.Branch;
import org.springframework.data.repository.CrudRepository;

public interface BranchRepository extends CrudRepository<Branch, Long> {

}
