package com.loyaltyrewards.repositories.point;

import com.loyaltyrewards.models.point.EarnedPoint;
import org.springframework.data.repository.CrudRepository;


public interface EarnedPointRepository extends CrudRepository<EarnedPoint, Long> {
}
