package com.loyaltyrewards.lib;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Date;

@Component
public class MongoDB {
    public void insertToMongoDB(String collection, Long customer_id, String  page, String page_id) {
        MongoClient mongo = new MongoClient( "localhost" , 27017 );
        //Connecting to the database
        MongoDatabase database = mongo.getDatabase("loyaltyprogrammongodb");
        //Creating a collection
        try {
            database.createCollection(collection);
        } catch (Exception e) {
            System.out.println("Mongo Error");
        }
        //Preparing a document
        Document document = new Document();
        document.append("customer_id", customer_id);
        document.append("page", page);
        document.append("page_id", page_id);
        Date date= new Date();
        long time = date.getTime();
        Timestamp ts = new Timestamp(time);
        document.append("time", ts);
        //Inserting the document into the collection
        database.getCollection(collection).insertOne(document);
        System.out.println(document);
        System.out.println("Document inserted successfully");
    }
}
