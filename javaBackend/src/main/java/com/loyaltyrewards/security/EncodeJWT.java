package com.loyaltyrewards.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.json.JSONObject;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EncodeJWT {

    public static String getJWTToken(Long id, String username, Long customer_id, Long agent_id, String lastLogin) {
        String secretKey = "mySecretKey";
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList("ROLE_USER,"+id.toString()+","+username+","+customer_id+","+agent_id);
        JSONObject payload=new JSONObject();
        payload.put("id",id);
        payload.put("username",username);
        payload.put("customer_id",customer_id);
        payload.put("agent_id",agent_id);
        payload.put("lastLogin",lastLogin);
        String token = Jwts
                .builder()
                .setPayload(payload.toString())
                .signWith(SignatureAlgorithm.HS512,
                        secretKey.getBytes()).compact();
        return token;
    }
}
