package com.loyaltyrewards.schedule;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.loyaltyrewards.services.SQLService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
public class SchedulerTask {

    @Autowired
    SQLService sqlService;

    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

//    @Scheduled(fixedRate = 10000)
//    public void timerRate() {
//        System.out.println(dateFormat.format(new Date()));
//    }
//
//    //第一次延遲1秒執行，當執行完後2秒再執行
//    @Scheduled(initialDelay = 1000, fixedDelay = 2000)
//    public void timerInit() {
//        System.out.println("init : "+dateFormat.format(new Date()));
//    }

    //每天23點59分59秒執行  every day (cron = "ss mm hh * * ?")
    @Scheduled(cron = "59 59 23 * * ?")
    public void timerCron() {
        System.out.println("Check Expired points: "+ dateFormat.format(new Date()));
        String date = java.time.LocalDate.now().toString();

        String sql = String.format("SELECT " +
                        "points," +
                        "expired_date, " +
                        "customer_id " +
                        "from earned_point " +
                        "where (CAST(expired_date AS DATE) = '%1$s')",date);
        List<JSONObject> result = this.sqlService.selectQuery2(sql);
        System.out.println(result);
        System.out.println(result);
        if (result.size() > 0) {
            for (int i = 0; i < result.size() ; i++) {
                String points = result.get(i).getString("points");
                String customer_id = result.get(i).getString("customer_id");

                String tableName = "expired_point";
                String columnName = "points,customer_id";
                String columnData = String.format("%1$s,%2$s", points, customer_id);
                // SQL Injection
                sqlService.insertQuery(tableName, columnName, columnData);
            }
        }
    }
}
