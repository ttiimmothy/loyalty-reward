package com.loyaltyrewards.models.agent;

import com.loyaltyrewards.models.user.User;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Entity(name="agents")
public class Agent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name="user_id")
    private User users;

    @Column(nullable = false)
    private Date employed_data;

    @Column(columnDefinition = "boolean default false")
    private Boolean is_deleted;

    @Column(columnDefinition = "integer default 1")
    private Integer level;

    @Column(columnDefinition = "boolean default true")
    private Boolean notification;

    @Column(columnDefinition = "integer default 200")
    private Integer each_appointment_commission;

    @Column(columnDefinition = "integer default 100")
    private Integer each_referral_commission;

    @OneToMany(mappedBy = "agent")
    private List<AgentContact> agentContact;

    @OneToMany(mappedBy = "agent")
    private List<AppointmentCommission> appointmentCommission;

    @OneToMany(mappedBy = "agent")
    private List<ReferralCommission> referralCommissions;

    @OneToMany(mappedBy = "agent")
    private List<ColdCallHistory> coldCallHistory;

    @Column(name="create_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP",updatable = false)
    @Generated(GenerationTime.INSERT)
    private Timestamp createTime;

    @Column(name="update_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Generated(GenerationTime.INSERT)
    private Timestamp updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUsers() {
        return users;
    }

    public void setUsers(User users) {
        this.users = users;
    }

    public Date getEmployed_data() {
        return employed_data;
    }

    public void setEmployed_data(Date employed_data) {
        this.employed_data = employed_data;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Boolean getNotification() {
        return notification;
    }

    public void setNotification(Boolean notification) {
        this.notification = notification;
    }

    public Integer getEach_appointment_commission() {
        return each_appointment_commission;
    }

    public void setEach_appointment_commission(Integer each_appointment_commission) {
        this.each_appointment_commission = each_appointment_commission;
    }

    public Integer getEach_referral_commission() {
        return each_referral_commission;
    }

    public void setEach_referral_commission(Integer each_referral_commission) {
        this.each_referral_commission = each_referral_commission;
    }

    public List<AgentContact> getAgentContact() {
        return agentContact;
    }

    public void setAgentContact(List<AgentContact> agentContact) {
        this.agentContact = agentContact;
    }

    public List<AppointmentCommission> getAppointmentCommission() {
        return appointmentCommission;
    }

    public void setAppointmentCommission(List<AppointmentCommission> appointmentCommission) {
        this.appointmentCommission = appointmentCommission;
    }

    public List<ReferralCommission> getReferralCommissions() {
        return referralCommissions;
    }

    public void setReferralCommissions(List<ReferralCommission> referralCommissions) {
        this.referralCommissions = referralCommissions;
    }

    public Boolean getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(Boolean is_deleted) {
        this.is_deleted = is_deleted;
    }

    public List<ColdCallHistory> getColdCallHistory() {
        return coldCallHistory;
    }

    public void setColdCallHistory(List<ColdCallHistory> coldCallHistory) {
        this.coldCallHistory = coldCallHistory;
    }
}
