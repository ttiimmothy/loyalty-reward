package com.loyaltyrewards.models.point;

import com.loyaltyrewards.models.treatment.Appointment;
import com.loyaltyrewards.models.user.Customer;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity(name="earned_point")
public class EarnedPoint {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name="customer_id")
    private Customer customer;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date expired_date;

    @Column(nullable = false)
    private Integer points;

    @OneToOne
    @JoinColumn(name = "appointment_id")
    private Appointment appointment;

    private String remark;

    @Column(name="create_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP",updatable = false)
    @Generated(GenerationTime.INSERT)
    private Timestamp createTime;

    @Column(name="update_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Generated(GenerationTime.INSERT)
    private Timestamp updateTime;

    @Column(columnDefinition = "boolean default false")
    private Boolean is_deleted;
}
