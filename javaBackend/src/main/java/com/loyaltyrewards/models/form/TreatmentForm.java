package com.loyaltyrewards.models.form;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public class TreatmentForm {

    private String default_score;
    private String detail;
    private String image;
    private Boolean is_hidden;
    private Integer purchase_benefit_condition;
    private Integer purchase_benefit_amount;
    private String title;
    private MultipartFile file;
    private List<Integer> gender;
    private List<Integer> branch;
    private List<Integer> service;


    public String getDefault_score() {
        return default_score;
    }

    public void setDefault_score(String default_score) {
        this.default_score = default_score;
    }

    public Boolean getIs_hidden() {
        return is_hidden;
    }

    public void setIs_hidden(Boolean is_hidden) {
        this.is_hidden = is_hidden;
    }

    public List<Integer> getGender() {
        return gender;
    }

    public void setGender(List<Integer> gender) {
        this.gender = gender;
    }

    public List<Integer> getBranch() {
        return branch;
    }

    public void setBranch(List<Integer> branch) {
        this.branch = branch;
    }

    public List<Integer> getService() {
        return service;
    }

    public void setService(List<Integer> service) {
        this.service = service;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Integer getPurchase_benefit_condition() {
        return purchase_benefit_condition;
    }

    public void setPurchase_benefit_condition(Integer purchase_benefit_condition) {
        this.purchase_benefit_condition = purchase_benefit_condition;
    }

    public Integer getPurchase_benefit_amount() {
        return purchase_benefit_amount;
    }

    public void setPurchase_benefit_amount(Integer purchase_benefit_amount) {
        this.purchase_benefit_amount = purchase_benefit_amount;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
