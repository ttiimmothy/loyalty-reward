package com.loyaltyrewards.models.form;

import java.util.Date;
import java.util.UUID;

public class AgentForm {
        private Long id;
        private Integer each_appointment_commission;
        private Integer each_referral_commission;
        private Boolean is_deleted;
        private String employed_data;
        private Integer level;

        public Integer getLevel() {
                return level;
        }

        public void setLevel(Integer level) {
                this.level = level;
        }

        public String getEmployed_data() {
                return employed_data;
        }

        public void setEmployed_data(String employed_data) {
                this.employed_data = employed_data;
        }

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }

        public Integer getEach_appointment_commission() {
                return each_appointment_commission;
        }

        public void setEach_appointment_commission(Integer each_appointment_commission) {
                this.each_appointment_commission = each_appointment_commission;
        }

        public Integer getEach_referral_commission() {
                return each_referral_commission;
        }

        public void setEach_referral_commission(Integer each_referral_commission) {
                this.each_referral_commission = each_referral_commission;
        }

        public Boolean getIs_deleted() {
                return is_deleted;
        }

        public void setIs_deleted(Boolean is_deleted) {
                this.is_deleted = is_deleted;
        }
}
