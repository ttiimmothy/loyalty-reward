package com.loyaltyrewards.models.form;

public class AppleLoginForm {

        private String clientId;
        private String identityToken;
        private String authorizationCode;

        public String getClientId() {
                return clientId;
        }

        public void setClientId(String clientId) {
                this.clientId = clientId;
        }

        public String getIdentityToken() {
                return identityToken;
        }

        public void setIdentityToken(String identityToken) {
                this.identityToken = identityToken;
        }

        public String getAuthorizationCode() {
                return authorizationCode;
        }

        public void setAuthorizationCode(String authorizationCode) {
                this.authorizationCode = authorizationCode;
        }
}
