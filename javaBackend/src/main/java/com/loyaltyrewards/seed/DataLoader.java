package com.loyaltyrewards.seed;

import com.loyaltyrewards.services.SQLService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;


@Component
public class DataLoader implements CommandLineRunner {

    @Autowired
    com.loyaltyrewards.lib.CSVReader CSVReader;

    @Autowired
    SQLService sqlService;

    @Override
    public void run(String... args) throws Exception {
//        String fileLocation= "src/main/java/com/loyaltyrewards/seed/csv/";
        String fileLocation= "csv/";
        String[] array = {
                "gender",
                "role",
                "register_status",
                "users",
                "customers",
                "branches",
                "treatments",
                "services",
                "treatment_service_relation",
                "treatment_branch_relation",
                "gender_treatments_relation",
                "advertisements",
                "appointments",
                "pickup_location",
                "reward_type",
                "rewards",
                "pickup_location_reward_relation",
                "cart_item",
                "redeem_items",
                "redeem_order",
                "expired_point",
                "earned_point",
                "scores",

                "enquiry",

                "agents",

                "agent_contact",
                "referral_commission",
                "appointment_commission",
                "cold_call_history",
                "result",
                "admin",
                "python_group",
                "purchased_item"

        };
        List<String> list = Arrays.asList(array);


        String fileType= "csv";

        for (int i = 0; i < list.size(); i = i + 1) {
            String tableName= list.get(i);
            String sql = String.format("SELECT id FROM %1$s", tableName);
            if (sqlService.selectQuery(sql).size() == 0 ) {
                System.out.println(sqlService.selectQuery(sql).size() );
                CSVReader.main(args, fileLocation, tableName, fileType);
            }
        }
    }
}
